// $Id$

/**
 * @file
 * Handler classes of the editor's toolbar.
 *
 * Copyright 2010 by Department of Software Engineering, University of Szeged, Hungary
 */

var Dtc = Dtc || {};

(function ($) {
  /**
   * Determines if the element is fully viewable.
   *
   * @param elem
   * @returns {Boolean}
   */
  Dtc.isScrolledIntoView = function (elem) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemBottom >= docViewTop) && (elemTop <= docViewBottom)
        && (elemBottom <= docViewBottom) && (elemTop >= docViewTop));
  };

  /**
   * Converts a color from string to hexadecimal format. Possible values of
   * color: - rgb(0,0,0) - rgba(0,0,0,0) - #000000
   *
   * @param color
   *          The color we want to convert.
   * @returns The color in hexadecimal format.
   */
  Dtc.colorToHex = function (color) {
    if (color.substring(0, 1) === '#') {
      return color;
    }
    color = color.replace(/[rgba() ]/g, "").split(",");

    var red = parseInt(color[0]);
    var green = parseInt(color[1]);
    var blue = parseInt(color[2]);

    var rgb = blue | (green << 8) | (red << 16);
    rgb = rgb.toString(16);
    if (rgb.length == 1) {
      rgb = "000000";
    }

    return "#" + rgb;
  };

  Dtc.borderRadius = {
    'border-top-left-radius' : {
      'webkit' : '-webkit-border-top-left-radius',
      'moz' : '-moz-border-radius-topleft'
    },
    'border-top-right-radius' : {
      'webkit' : '-webkit-border-top-right-radius',
      'moz' : '-moz-border-radius-topright'
    },
    'border-bottom-left-radius' : {
      'webkit' : '-webkit-border-bottom-left-radius',
      'moz' : '-moz-border-radius-bottomleft'
    },
    'border-bottom-right-radius' : {
      'webkit' : '-webkit-border-bottom-right-radius',
      'moz' : '-moz-border-radius-bottomright'
    }
  };

  /**
   * [START] Toolbar
   */
  Dtc.Toolbar = Class.create();
  Dtc.Toolbar.prototype = {
    initialize : function () {
      this.state = 'visible';
      this.dock = false;
      this.positionBeforeDock = null;
      this.elements = {};
      this.hovered = null;
    },
    resetStyle : function () {
      var toolbarCss = {
        'font-size' : '12px',
        'font-family' : '"Lucida Grande", "Lucida Sans", Arial, sans-serif',
        'font-color' : 'black',
        'font-weight' : 'normal',
        'float' : 'none',
        'text-decoration' : 'none',
        'text-direction' : 'ltr',
        'text-transform' : 'none',
        'background-color' : '#ffffff',
        'line-height' : '16px',
        'word-spacing' : '0px',
        'letter-spacing' : '0px',
        'text-indent' : '0px',
        'clear' : 'none',
        'padding' : '0px 0px 0px 0px',
        'margin' : '0px 0px 0px 0px'
      };
      $('#dtc-toolbar').parents().find('.ui-dialog').css(toolbarCss);
      $('.ui-dialog:has(#dtc-toolbar)').find('div').css('text-decoration',
          'none');
    },
    /**
     * Moves the toolbar.
     */
    moveToolbar : function () {
      if (Dtc.isScrolledIntoView('.ui-dialog:has(#dtc-toolbar)') || this.dock) {
        return;
      }

      var toolbarDialog = $('.ui-dialog:has(#dtc-toolbar)');
      var offset = toolbarDialog.offset();
      var pos = ($(window).height() - toolbarDialog.height()) / 2
          + $(window).scrollTop();
      pos = (pos < 0) ? 0 : pos;
      toolbarDialog.animate({
        "top" : pos + "px"
      }, "slow");
    },
    /**
     * Helper method for minimizing toolbar.
     */
    switchState : function () {
      if (this.state == 'visible') {
        $('#minimize-toolbar').button('option', {
          icons : {
            primary : 'ui-icon-circle-triangle-s'
          }
        });
        this.state = 'hidden';
      }
      else {
        $('#minimize-toolbar').button('option', {
          icons : {
            primary : 'ui-icon-circle-triangle-n'
          }
        });
        this.state = 'visible';
      }
    },
    dockStateSwitcher : function () {
      if (this.dock) {
        $('#dtc-toolbar').dialog('option', 'width', 620);
        $('#dtc-toolbar').dialog('option', 'draggable', true);
        $("#toolbar-content")
            .removeClass('ui-tabs-vertical ui-helper-clearfix');
        $("#toolbar-content li").addClass('ui-corner-top').removeClass('ui-corner-left');

        $('.ui-dialog:has(#dtc-toolbar)').css('position', 'absolute');

        $('#dtc-toolbar').dialog('option', 'position', this.positionBeforeDock);

        this.dock = false;

        $('body').css('margin-bottom', '0');

        $('#dock-toolbar').button('option', {
          icons : {
            primary : 'ui-icon-arrowthick-1-sw'
          }
        });
        $('.ui-dialog:has(#dtc-toolbar)').css('bottom', '');
      }
      else {
        // Note which tab was selected when the action performed.
        var selected = $('#toolbar-content').tabs('option', 'selected');

        // Select the tab width the largest width.
        $('#toolbar-content').tabs('select', 'toolbar-border-settings');

        // Store the height of the window.
        var minheight = $('.ui-dialog:has(#dtc-toolbar)').height();

        // Set this to the minimum height.
        $('#dtc-toolbar').dialog('option', 'minHeight', minheight);

        // Fix an resize the toolbar window.
        $('#dtc-toolbar').dialog('option', 'width', "100%");
        this.positionBeforeDock = $('#dtc-toolbar')
            .dialog('option', 'position');
        $('.ui-dialog:has(#dtc-toolbar)').css('position', 'fixed');
        $('.ui-dialog:has(#dtc-toolbar)').css('top', '');
        $('.ui-dialog:has(#dtc-toolbar)').css('left', 0);
        $('.ui-dialog:has(#dtc-toolbar)').css('bottom', 0);
        $('#dtc-toolbar').dialog('option', 'draggable', false);
        $('#dtc-toolbar').css("min-height", "255px");
        this.dock = true;

        // Set back the original tab selection.
        $('#toolbar-content').tabs('option', 'selected', selected);

        // Change the dock button style.
        $('#dock-toolbar').button('option', {
          icons : {
            primary : 'ui-icon-arrowthick-1-ne'
          }
        });
        $("#toolbar-content").addClass('ui-tabs-vertical ui-helper-clearfix');
        $("#toolbar-content li").removeClass('ui-corner-top').addClass(
            'ui-corner-left');
      }
      $(".route-active").click();

      if (this.state == 'hidden') {
        this.switchState();
      }
    },
    minimize : function () {
      if (Dtc.toolbar.state == 'visible') {
        if (Dtc.toolbar.dock) {
          var h = ($("#dtc-toolbar").height()) + 15;
          $('.ui-dialog:has(#dtc-toolbar)').animate({
            "bottom" : -1 * h + "px"
          }, "slow");
          $('body').css('margin-bottom', 50 + 'px');
        }
        else {
          var options = {};
          $('#dtc-toolbar').hide('blind', options, 400);
        }
      }
      else {
        if (Dtc.toolbar.dock) {
          $('.ui-dialog:has(#dtc-toolbar)').animate({
            "bottom" : 0 + "px"
          }, "slow");
          $('body').css(
              'margin-bottom',
              ($(window).height() - $('.ui-dialog:has(#dtc-toolbar)').height())
                  + "px");
        }
        else {
          $('#dtc-toolbar').show('blind', options, 400);
        }
      }
      Dtc.toolbar.switchState();
    },
    addMinimizeing : function () {
      $(".ui-dialog-titlebar").dblclick(function (e) {
        if ($(e.target).hasClass("ui-dialog-titlebar"))
          Dtc.toolbar.minimize();
      });
      $(".ui-dialog-titlebar").mousedown(function () {
        $("#dtc-toolbar").find(".color").blur();
      });
      // $('#minimize').click(this.minimize);
    },
    /**
     * Method for popping up a dialog if no items have been selected.
     */
    handleNoSelection : function () {
      $('#no-selected-item').dialog({
        modal : true,
        autoOpen : false,
        buttons : {
          Ok : function () {
            $(this).dialog('close');
          }
        }
      });
      $('.toolbar-element').click(function (event) {
        if (Dtc.editor.currentlyEditing['tag'] == '') {
          $('#no-selected-item').dialog('open');
          event.stopImmediatePropagation();
        }
      });
    },
    /**
     * Creates toolbar
     */
    create : function () {
      var self = this;
      $('#toolbar-content').tabs();
      $('#dtc-toolbar').dialog({
        autofit : true,
        resizable : false,
        open : self.resetStyle,
        width : 620
      });
    },
    /**
     * Changes the toolbar's close icon
     */
    removeCloseIcon : function () {
      $('.ui-dialog-titlebar-close').remove();
    },
    /**
     * Creates and initializes some additional buttons on the toolbar.
     */
    enableHeaderButtons : function () {
      var header = $(".ui-dialog:has(#dtc-toolbar)").find(
          "#ui-dialog-title-dtc-toolbar");
      header.css("float", "left");
      header.after(Drupal.theme('dtcHeaderButtons'));

      $('#clear-all').button();
      $('#generate').button();
      $('#minimize-toolbar').button();
      $('#minimize-toolbar').button('option',{
        label : 'dock',
        icons : {
          primary : 'ui-icon-circle-triangle-n'
        },
        text : false
      }).click(this.minimize);
      $('#dock-toolbar').button();
      $('#dock-toolbar').button('option', {
        label : 'dock',
        icons : {
          primary : 'ui-icon-arrowthick-1-sw'
        },
        text : false
      });
      $('#redo').button();
      $('#redo').button('option', {
        icons : {
          primary : 'ui-icon-arrowreturnthick-1-e'
        },
        text : false
      });
      $('#undo').button();
      $('#undo').button('option', {
        icons : {
          primary : 'ui-icon-arrowreturnthick-1-w'
        },
        text : false
      });
      $('#dock-toolbar').click(function (e) {
        Dtc.toolbar.dockStateSwitcher();
      });
      $('#clear-all').click(function (e) {
        $("body").append(Drupal.theme('dtcClearDialog'));
        $("#clear-dialog-confirm").dialog({
          resizable : false,
          height : 140,
          modal : true,
          buttons : {
            'Delete' : function () {
              $(this).dialog('close');
              $("#clear-dialog-confirm").dialog("destroy");
              $("#clear-dialog-confirm").remove();
              // Delete cookie.
              if (!Dtc.editorOnly) {
                $.cookie($.cookie("dtc_basetheme") + "-cookie", null);
                Dtc.reload.popups = 0;
              }
              else {
                $.ajax({
                  async : false,
                  type : 'GET',
                  url : Drupal.settings.basePath + "dtc/editor/clear"
                });
                Dtc.config.modified = false;
              }
              location.reload();
            },
            Cancel : function () {
              $(this).dialog('close');
              $("#clear-dialog-confirm").dialog("destroy");
              $("#clear-dialog-confirm").remove();
            }
          }
        });
      });
      $('#toolbar-opacity-slider').slider({
        range : "min",
        value : 0,
        min : 0,
        max : 60,
        step : 10,
        slide : function (event, ui) {
          var opacity = 100 - ui.value;
          var classString = $(".ui-dialog:has(#dtc-toolbar)").attr("class");
          classString = classString.replace(/ toolbar-opacity-[0-9]{1,3}/, "");
          classString += " toolbar-opacity-" + opacity;
          $(".ui-dialog:has(#dtc-toolbar)").attr("class", classString);
        }
      });
      
      $('#turn-off').button();
      $('#turn-off').click(function (e) {
        e.preventDefault();
        window.location.href = Drupal.settings.basePath + "dtc/editor/turn/off";
      });
      
      $(".selector-button").button();
      $("#class-selector").selectmenu({
        style : 'dropdown',
        width : 200
      });
      $("#class-selector").selectmenu("disable");
      $(".selector").button("disable");
      $('#status-select').selectmenu({
        style : 'dropdown',
        width : 80
      });
      $('#apply-button').button({
        disabled : true
      });
      $('#status-select-menu').addClass('smoothness');
    },
    enableToolbarButtons : function () {
      $('.lock-checkbox').button();
      $('.lock-checkbox').button('option', {
        icons : {
          primary : 'ui-icon-unlocked'
        },
        text : false
      });
      $('.lock-checkbox').change(function () {
        if ($(this).is(':checked')) {
          $(this).button('option', 'icons', {
            primary : 'ui-icon-locked'
          });
        }
        else {
          $(this).button('option', 'icons', {
            primary : 'ui-icon-unlocked'
          });
        }
      });

      $('#text-decoration').buttonset();
      $('#text-direction').buttonset();
      $('#background-attachment').buttonset();
      $('#background-repeat').buttonset();
      $('#text-valign').buttonset();
      $('#text-align').buttonset();
      $('#text-transform').buttonset();
      $('#restore-hidden').button();

      $("#text-decoration label[for='edit-text-decoration-font-weight-bold'] span").css("font-weight", "bold");
      $("#text-decoration label[for='edit-text-decoration-font-style-italic'] span").css(
          "font-style", "italic");
      $("#text-decoration label[for='edit-text-decoration-text-decoration-underline'] span").css("text-decoration",
          "underline");
      $("#text-decoration label[for='edit-text-decoration-text-decoration-overline'] span").css("text-decoration",
          "overline");
      $("#text-decoration label[for='edit-text-decoration-text-decoration-line-through'] span").css(
          "text-decoration", "line-through");
      $("#text-decoration label[for='edit-text-decoration-text-decoration-blink'] span").css("text-decoration",
          "blink");
      $("input.color").click(function () {
        $("body > div:last").addClass("no-hover");
      });
    },
    disableToolbarButtons : function () {
      $(".lock-checkbox").unbind("change");
      $(".lock-checkbox").button("destroy");
      $("#text-transform input").button("destroy");
      $("#text-direction input").button("destroy");
      $("#background-attachment input").button("destroy");
      $("#background-repeat input").button("destroy");
      $("#text-valign input").button("destroy");
      $("#text-align input").button("destroy");
      $('#text-decoration input').button("destroy");
      $('#restore-hidden').button("destroy");
    },
    /**
     * Add events to the path to be able to scroll it.
     */
    makePathScrollable : function () {
      var stop = false;
      $("#dtc-scroll-left").hover(function (e) {
        var dtcPath = $("#dtc-path");
        var left = parseInt(dtcPath.css("left").replace("px", ""));
        if (left < 0 && dtcPath.children().length > 0) {
          $(this).addClass("scroll-left-cursor");
          stop = false;
          var scroll = setTimeout(function () {
            left = parseInt(dtcPath.css("left").replace("px", ""));
            if (left < 0 && !stop) {
              dtcPath.css("left", (left + 5) + "px");
              setTimeout(arguments.callee, 50);
            }
          }, 50);
        }
      }, function (e) {
        $(this).removeClass("scroll-left-cursor");
        stop = true;
      });
      $("#dtc-scroll-right").hover(function (e) {
        var dtcPath = $("#dtc-path");
        var dtcPathContent = dtcPath.children();
        if (dtcPathContent.length > 0) {
          var pathWidth = $("#dtc-path").innerWidth();
          var left = parseInt(dtcPath.css("left").replace("px", ""));
          var width = 0;
          dtcPathContent.each(function () {
            width += $(this).outerWidth();
          });
          width += 20;
          if (left + width > pathWidth) {
            $(this).addClass("scroll-right-cursor");
            stop = false;
            var scroll = setTimeout(function () {
              left = parseInt(dtcPath.css("left").replace("px", ""));
              if (left + width > pathWidth && !stop) {
                dtcPath.css("left", (left - 5) + "px");
                setTimeout(arguments.callee, 50);
              }
            }, 50);
          }
        }
      }, function (e) {
        $(this).removeClass("scroll-right-cursor");
        stop = true;
      });
    },
    /**
     * Adds a toolbar element to the toolbar.
     *
     * @param prop
     * @param element
     */
    add : function (prop, element) {
      this.elements[prop] = element;
    },
    /**
     * Gets a toolbar element from the toolbar.
     *
     * @param prop
     */
    get : function (prop) {
      return this.elements[prop];
    },
    /**
     * Resets the states of all toolbar's element
     */
    reset : function () {
      for ( var i in this.elements) {
        this.elements[i].reset();
      }
    },
    createElements : function () {
      // Basic.
      this.add("background-color", new Dtc.Toolbar.DtcColorWidget("background-color"));
      this.add("font-style", new Dtc.Toolbar.DtcToggleWidget("font-style", "italic"));
      this.add("font-weight", new Dtc.Toolbar.DtcFontWeight());

      // Font settings.
      this.add("font-size", new Dtc.Toolbar.DtcSliderWidget("font-size", {value : 10, min : 6, max : 36, step : 1}));
      this.add("font-family", new Dtc.Toolbar.DtcDropDownWidget("font-family", "Georgia, serif"));
      this.add("color", new Dtc.Toolbar.DtcColorWidget("color"));

      // Border settings.
      this.add("border-left-width", new Dtc.Toolbar.DtcBorderWidth("border-left-width", {value : 0, min : 0, max : 10, step : 1}));
      this.add("border-left-style", new Dtc.Toolbar.DtcBorderStyle("border-left-style", "solid"));
      this.add("border-left-color", new Dtc.Toolbar.DtcBorderColor("border-left-color"));

      this.add("border-top-width", new Dtc.Toolbar.DtcBorderWidth("border-top-width", {value : 0, min : 0, max : 10, step : 1}));
      this.add("border-top-style", new Dtc.Toolbar.DtcBorderStyle("border-top-style", "solid"));
      this.add("border-top-color", new Dtc.Toolbar.DtcBorderColor("border-top-color"));

      this.add("border-bottom-width", new Dtc.Toolbar.DtcBorderWidth("border-bottom-width", {value : 0, min : 0, max : 10, step : 1}));
      this.add("border-bottom-style", new Dtc.Toolbar.DtcBorderStyle("border-bottom-style", "solid"));
      this.add("border-bottom-color", new Dtc.Toolbar.DtcBorderColor("border-bottom-color"));

      this.add("border-right-width", new Dtc.Toolbar.DtcBorderWidth("border-right-width", {value : 0, min : 0, max : 10, step : 1}));
      this.add("border-right-style", new Dtc.Toolbar.DtcBorderStyle("border-right-style", "solid"));
      this.add("border-right-color", new Dtc.Toolbar.DtcBorderColor("border-right-color"));

      this.add("border-style-lock",
          new Dtc.Toolbar.DtcBorderStyleLock(
            'border-style-lock',
            this.get("border-top-style"),
            this.get("border-bottom-style"),
            this.get("border-left-style"),
            this.get("border-right-style")
          )
      );
      this.add("border-width-lock",
          new Dtc.Toolbar.DtcSliderLockWidget(
            'border-style-lock',
            this.get("border-top-width"),
            this.get("border-bottom-width"),
            this.get("border-left-width"),
            this.get("border-right-width")
          )
      );

      this.add("border-top-left-radius", new Dtc.Toolbar.DtcBorderRadiusSliderWidget("border-top-left-radius"));
      this.add("border-top-right-radius", new Dtc.Toolbar.DtcBorderRadiusSliderWidget("border-top-right-radius"));
      this.add("border-bottom-left-radius", new Dtc.Toolbar.DtcBorderRadiusSliderWidget("border-bottom-left-radius"));
      this.add("border-bottom-right-radius", new Dtc.Toolbar.DtcBorderRadiusSliderWidget("border-bottom-right-radius"));
      this.add("border-radius-lock",
          new Dtc.Toolbar.DtcSliderLockWidget(
            "border-radius-lock",
            this.get("border-top-left-radius"),
            this.get("border-top-right-radius"),
            this.get("border-bottom-left-radius"),
            this.get("border-bottom-right-radius")
          )
      );

      // Layout.
      this.add("text-align", new Dtc.Toolbar.DtcRadioWidget("text-align", "left"));
      this.add("vertical-align", new Dtc.Toolbar.DtcRadioWidget("vertical-align", "top"));

      // Advanced.
      this.add("text-decoration", new Dtc.Toolbar.DtcTextDecoration());
      this.add("text-transform", new Dtc.Toolbar.DtcRadioWidget("text-transform", "none"));
      this.add("direction", new Dtc.Toolbar.DtcRadioWidget("direction", "ltr"));
      this.add("line-height", new Dtc.Toolbar.DtcSliderWidget("line-height"));
      this.add("word-spacing", new Dtc.Toolbar.DtcSliderWidget("word-spacing"));
      this.add("letter-spacing", new Dtc.Toolbar.DtcSliderWidget("letter-spacing"));
      this.add("text-indent", new Dtc.Toolbar.DtcSliderWidget("text-indent"));

      this.add("padding-left", new Dtc.Toolbar.DtcSliderWidget("padding-left",  {value : 0, min : 0, max : 60, step : 1}));
      this.add("padding-bottom", new Dtc.Toolbar.DtcSliderWidget("padding-bottom", {value : 0, min : 0, max : 60, step : 1}));
      this.add("padding-right", new Dtc.Toolbar.DtcSliderWidget("padding-right", {value : 0, min : 0, max : 60, step : 1}));
      this.add("padding-top", new Dtc.Toolbar.DtcSliderWidget("padding-top", {value : 0, min : 0, max : 60, step : 1}));
      this.add("padding-lock",
          new Dtc.Toolbar.DtcSliderLockWidget(
            'padding-lock',
            this.get('padding-left'),
            this.get('padding-right'),
            this.get('padding-bottom'),
            this.get('padding-top')
          )
      );

      this.add("margin-left", new Dtc.Toolbar.DtcSliderWidget("margin-left", {value : 0, min : 0, max : 60, step : 1}));
      this.add("margin-bottom", new Dtc.Toolbar.DtcSliderWidget("margin-bottom", {value : 0, min : 0, max : 60, step : 1}));
      this.add("margin-right", new Dtc.Toolbar.DtcSliderWidget("margin-right", {value : 0, min : 0, max : 60, step : 1}));
      this.add("margin-top", new Dtc.Toolbar.DtcSliderWidget("margin-top", {value : 0, min : 0, max : 60, step : 1 }));
      this.add("margin-lock",
          new Dtc.Toolbar.DtcSliderLockWidget(
            'margin-lock',
            this.get('margin-left'),
            this.get('margin-right'),
            this.get('margin-bottom'),
            this.get('margin-top')
          )
      );

      this.add("width", new Dtc.Toolbar.DtcWidthHeightWidget("width", {value : 0, min : 0, max : 1000, step : 1}));
      this.add("height", new Dtc.Toolbar.DtcWidthHeightWidget("height", {value : 0, min : 0, max : 1000, step : 1}));
      this.add("background-image", new Dtc.Toolbar.DtcBackgroundImage("background-image"));
      this.add("background-attachment", new Dtc.Toolbar.DtcRadioWidget("background-attachment", "scroll"));
      this.add("background-repeat", new Dtc.Toolbar.DtcRadioWidget("background-repeat", "repeat"));
      if (!$.browser.msie) {
        this.add("background-position", new Dtc.Toolbar.DtcBackgroundPosition());
      }
      else {
        this.add("background-position-x", new Dtc.Toolbar.DtcBackgroundPosition("x"));
        this.add("background-position-y", new Dtc.Toolbar.DtcBackgroundPosition("y"));
      }
      this.add("float", new Dtc.Toolbar.DtcDropDownWidget("float", "none"));
      this.add("clear", new Dtc.Toolbar.DtcDropDownWidget("clear", "none"));
      this.add("display", new Dtc.Toolbar.DtcDisplay());
    },
    disableElements : function () {
      for ( var i in this.elements) {
        this.elements[i].disable();
      }
    },
    enableElements : function () {
      for ( var i in this.elements) {
        this.elements[i].enable();
      }
    },
    /**
     * Initializes toolbar.
     *
     * @returns
     */
    enable : function () {
      this.handleNoSelection();
      this.create();
      this.removeCloseIcon();
      this.enableHeaderButtons();
      this.enableToolbarButtons();
      this.makePathScrollable();
      this.createElements();
      this.addMinimizeing();
    },
    hideToolbarError : function () {
      $("#toolbar-error").fadeOut("fast");
    },
    showToolbarError : function (message) {
      $("#toolbar-error").text(message);
      $("#toolbar-error").fadeIn("slow");
    }
  };

  /**
   * Adds the fucntionality to color pickers.
   *
   * @param property
   *          The CSS property handled by this widget.
   */
  Dtc.Toolbar.DtcColorWidget = Class.create();
  Dtc.Toolbar.DtcColorWidget.prototype = {
    initialize : function (property) {
      this.property = property;
      this.fieldSelector = "input[name='" + this.property + "']";
      var self = this;
      $("input[name='" + this.property + "']").change(function () {
        self.colorChange();
      });
    },
    colorChange : function () {
      var obj = this.fieldSelector;
      var value = $(obj).val();
      if (value == "") {
        $(obj)[0].color.fromString("ffffff");
      }
      Dtc.showPreview($(obj)[0].name, value);
    },
    disable : function () {
      $(this.fieldSelector).unbind("change");
    },
    enable : function () {
      this.initialize(this.property);
    },
    setValue : function (color) {
      if (color == "#d8f1f8" || color == "rgb(216, 241, 248)") {
        color = Dtc.fade.savedBackground;
      }

      if (!color || color == "transparent" || color == "undefined" || color.length == 0 || color == "rgba(0, 0, 0, 0)") {
        color = "#ffffff";
      }
      else {
        color = Dtc.colorToHex(color);
      }

      $("input[name='" + this.property + "']")[0].color.fromString(color
          .substring(1));
    },
    reset : function () {
      this.setValue("#ffffff");
    }
  };

  Dtc.Toolbar.DtcBorderColor = Class.create();
  Dtc.Toolbar.DtcBorderColor.prototype = {
    initialize : function (property) {
      this.property = property;
      var self = this;
      $("input[name='" + this.property + "']").change(
          function () {
            if ($('#border-style-lock').is(':checked')) {
              var sides = [ "top", "left", "bottom", "right" ];
              for ( var i in sides) {
                Dtc.toolbar.get("border-" + sides[i] + "-color").colorChange(
                    this.value);
                Dtc.toolbar.get("border-" + sides[i] + "-color").setValue(
                    this.value);
              }
            }
            else {
              self.colorChange(this.value);
            }

          });
    },
    colorChange : function (color) {
      var widthProperty = this.property.replace("color", "width");
      var styleProperty = this.property.replace("color", "style");
      var widthValue = $("#" + widthProperty + "-slider").slider("value");
      var styleValue = $("#" + styleProperty + "-field").val();
      if (widthValue == 0) {
        widthValue = 1;
        Dtc.toolbar.get(widthProperty).setValue(1);
      }

      Dtc.showPreview(this.property, color);
      Dtc.showPreview(widthProperty, widthValue + "px");
      Dtc.showPreview(styleProperty, styleValue);
    },
    setValue : function (color) {
      if (color == "#d8f1f8" || color == "rgb(216, 241, 248)") {
        color = Dtc.fade.savedBackground;
      }

      if (!color || color == "transparent" || color == "undefined" || color.length == 0 || color == "rgba(0, 0, 0, 0)") {
        color = "#ffffff";
      }
      else {
        color = Dtc.colorToHex(color);
      }

      $("input[name='" + this.property + "']")[0].color.fromString(color.substring(1));
    },
    reset : function () {
      this.setValue("#ffffff");
    },
    disable : function () {
      $("input[name='" + this.property + "']").unbind("change");
    },
    enable : function () {
      this.initialize(this.property);
    }
  };
  /**
   * Handles toggle buttons, like the italic button.
   *
   * @param property
   * @param toggledValue
   */
  Dtc.Toolbar.DtcToggleWidget = Class.create();
  Dtc.Toolbar.DtcToggleWidget.prototype = {
    initialize : function (property, toggledValue) {
      this.property = property;
      this.toggledValue = toggledValue;

      $("input[value*='" + property + "']").click(function () {
        var checked = $(this).attr("checked");
        var value = 'normal';
        if (checked) {
          value = toggledValue;
        }
        Dtc.showPreview(property, value);
      });
    },
    setValue : function (value) {
      var node = $("input[value*='" + this.property + "']");
      node.attr("checked", false);
      if (value == this.toggledValue) {
        node.attr('checked', true);
      }
      node.button("refresh");
    },
    reset : function () {
      this.setValue("normal");
    },
    disable : function () {
      $("input[value*='" + this.property + "']").unbind("click");
    },
    enable : function () {
      this.initialize(this.property, this.toggledValue);
    }
  };
  /**
   * Class for font-weight CSS property. It uses DtcToggleWidget as a base.
   */
  Dtc.Toolbar.DtcFontWeight = Class.create();
  Dtc.Toolbar.DtcFontWeight.prototype = {
    initialize : function () {
      this.widget = new Dtc.Toolbar.DtcToggleWidget("font-weight", "bold");
      this.reset = this.widget.reset;
    },
    setValue : function (value) {
      if (value == "normal" || value == "lighter" || parseInt(value) < 600) {
        this.widget.setValue("normal");
      }
      else {
        this.widget.setValue("bold");
      }
    },
    disable : function () {
      this.widget.disable();
    },
    enable : function () {
      this.widget.enable();
    }
  };

  /**
   * Handles the drop down elements in the tolbar.
   *
   * @param property
   *          The CSS property handled by this widget.
   * @param defaultValue
   *          The default value from the selectable elements.
   */
  Dtc.Toolbar.DtcDropDownWidget = Class.create();
  Dtc.Toolbar.DtcDropDownWidget.prototype = {
    initialize : function (property, defaultValue) {
      this.property = property;
      this.defaultValue = defaultValue;
      $("#" + property + "-field").change(function () {
        Dtc.showPreview(property, this.value);
      });
    },
    setValue : function (value) {
      // FIXME jquery do not find the option.
      $("#" + this.property + "-field").val(value);
    },
    reset : function () {
      this.setValue(this.defaultValue);
    },
    disable : function () {
      $("#" + this.property + "-field").unbind("change");
    },
    enable : function () {
      this.initialize(this.property, this.defaultValue);
    }
  };
  /**
   * Handles the border-stlye dropdowns intiutively.
   *
   * @param property
   *          The CSS property handled by this widget.
   * @param defaultValue
   *          The default value from the selectabel elements.
   */
  Dtc.Toolbar.DtcBorderStyle = Class.create();
  Dtc.Toolbar.DtcBorderStyle.prototype = {
    initialize : function (property, defaultValue) {
      this.property = property;
      var self = this;
      this.defaultValue = defaultValue;
      $("#" + property + "-field").change(function () {
        self.showBorderStylePreview($(this).val());
      });
    },
    setValue : function (value) {
      if (value == 'none') {
        value = 'solid';
      }

      $("#" + this.property + "-field").val(value);
    },
    reset : function () {
      this.setValue(this.defaultValue);
    },
    showBorderStylePreview : function (elementValue) {
      var widthProperty = this.property.replace("style", "width");
      var colorProperty = this.property.replace("style", "color");
      var widthValue = $("#" + widthProperty + "-slider").slider("value");
      if (widthValue == 0) {
        $("#" + widthProperty + "-slider").slider("value", 1);
        $("#" + widthProperty + "-field").text(1);
        widthValue = 1;
      }
      var color = $("input[name='" + colorProperty + "']").val();
      Dtc.showPreview(this.property, elementValue);
      Dtc.showPreview(widthProperty, widthValue + "px");
      Dtc.showPreview(colorProperty, color);
    },
    disable : function () {
      $("#" + this.property + "-field").unbind("change");
    },
    enable : function () {
      this.initialize(this.property, this.defaultValue);
    }
  };
  /**
   * Handles a group of buttons. Only one button can be selected. The buttons
   * have the same class.
   *
   * @param groupName
   *          The name of all buttons in a group.
   * @param defaultValue
   *          The default selected button's value.
   */
  Dtc.Toolbar.DtcButtonGroupWidget = Class.create();
  Dtc.Toolbar.DtcButtonGroupWidget.prototype = {
    initialize : function (groupName, defaultValue) {
      this.groupName = groupName;
      this.defaultValue = defaultValue;
      $('input[name="' + groupName + '"]').click(function () {
        var value = $(this).attr("value");
        Dtc.showPreview(this.name, value);
      });
    },
    setValue : function (value) {
      value = value.replace("start", defaultValue)
          .replace("auto", defaultValue);
      value = value.replace("baseline", defaultValue);
      $('input[name="' + this.groupName + '"]').attr('checked', false);
    },
    reset : function () {
      this.setValue(this.defaultValue);
    },
    disable : function () {
      $('input[name="' + this.groupName + '"]').unbind("click");
    },
    enable : function () {
      this.initialize(this.groupName, this.defaultValue);
    }
  };

  /**
   * Handles textfields on the toolbar.
   *
   * @param property
   *          The name of the property we will handle.
   */
  Dtc.Toolbar.DtcTextFieldWidget = Class.create();
  Dtc.Toolbar.DtcTextFieldWidget.prototype = {
    initialize : function (property) {
      this.property = property;
      $("#" + property + "-field").change(function () {
        Dtc.showPreview(property, this.value);
      });
    },
    setValue : function (value) {
      $("#" + this.property + "-field").val(value);
    },
    reset : function () {
      this.setValue('');
    },
    disable : function () {
      $("#" + this.property + "-field").unbind("change");
    },
    enable : function () {
      this.initialize(this.property);
    }
  };

  /**
   * Class for background image CSS property. It uses DtcToggleWidget as a base.
   */
  Dtc.Toolbar.DtcBackgroundImage = Class.create();
  Dtc.Toolbar.DtcBackgroundImage.prototype = {
    initialize : function () {
      this.widget = new Dtc.Toolbar.DtcTextFieldWidget("background-image");
      this.reset = this.widget.reset;
      $("#background-image-field").unbind("change");
      $("#background-image-field").change(function () {
        if (this.value.length != 0) {
          Dtc.showPreview("background-image", "url(" + this.value + ")");
        }
        else {
          Dtc.showPreview("background-image", "none");
        }
      });
    },
    setValue : function (value) {
      if (value.length != 0 && value != "none") {
        this.widget.setValue(value.substring(4, value.length - 1).replace(/"/g, ""));
      }
      else {
        this.widget.setValue("");
      }
    },
    disable : function () {
      $("#background-image-field").unbind("change");
      this.widget.disable();
    },
    enable : function () {
      this.initialize();
    }
  };
  /**
   * Handles radio buttons of the toolbar.
   *
   * @param property
   *          The CSS property handled by this widget.
   * @param defaultValue
   *          The default button.
   */
  Dtc.Toolbar.DtcRadioWidget = Class.create();
  Dtc.Toolbar.DtcRadioWidget.prototype = {
    initialize : function (property, defaultValue) {
      this.property = property;
      this.defaultValue = defaultValue;
      $("input[name='" + property + "']").change(
          function (event) {
            if (Dtc.editor.currentlyEditing['tag'] == '') {
              $('#no-selected-item').dialog('open');
              event.stopImmediatePropagation();
              $(this).removeAttr("checked");
              $("input[name='" + property + "'][value='" + defaultValue + "']").attr("checked", "checked");
              return false;
            }
            Dtc.showPreview(this.name, this.value);
          });
    },
    setValue : function (value) {
      $("input[name='" + this.property + "']").attr('checked', false);
      if ($("input[name='" + this.property + "'][value='" + value + "']").length != 0) {
        $("input[name='" + this.property + "'][value='" + value + "']").attr('checked', true);
      }
      else {
        this.reset();
      }
      $("input[name='" + this.property + "']").button("refresh");
    },
    reset : function () {
      this.setValue(this.defaultValue);
    },
    disable : function () {
      $("input[name='" + this.property + "']").unbind("change");
    },
    enable : function () {
      this.initialize(this.property, this.defaultValue);
    }
  };
  /**
   * Handles the Border radius widget.
   *
   * @param property
   *          The basic border-radius property.
   * @param settings
   *          Optional. The settings of the slider.
   */
  Dtc.Toolbar.DtcBorderRadiusSliderWidget = Class.create();
  Dtc.Toolbar.DtcBorderRadiusSliderWidget.prototype = {
    initialize : function (property, settings) {
      if (settings == null) {
        settings = {
          value : 0,
          min : 0,
          max : 30,
          step : 1
        };
      }
      this.property = property;
      this.settings = settings;

      var field = $('#' + property + '-field');
      var w = this;
      field.change(function () {
        w.setValue($(this).val());
        w.showBorderRadiusPreview($(this).val());
      });

      $('#' + property + '-slider').slider({
        range : "min",
        value : this.settings.value,
        min : this.settings.min,
        max : this.settings.max,
        step : this.settings.step,
        slide : function (event, ui) {
          if (Dtc.editor.currentlyEditing['tag'] == '') {
            $('#no-selected-item').dialog('open');
            event.stopImmediatePropagation();
            return false;
          }
          field.val(ui.value);
          w.showBorderRadiusPreview(ui.value);
        }
      });
    },
    setValue : function (value) {
      value = "" + value;
      value = value.split(' ');
      value = value[0].replace("px", "").replace("normal", "0");

      var field = $('#' + this.property + '-field');
      field.val(Math.floor(value));
      if (!$.browser.msie && !$.browser.opera) {
        $('#' + this.property + '-slider').slider("value", Math.floor(value));
      }
    },
    showBorderRadiusPreview : function (value) {
      Dtc.showPreview(this.property, value + "px");
      Dtc.showPreview(Dtc.borderRadius[this.property]['webkit'], value + "px");
      Dtc.showPreview(Dtc.borderRadius[this.property]['moz'], value + "px");
    },
    reset : function () {
      this.setValue(this.settings.value);
    },
    disable : function () {
      var field = $('#' + this.property + '-field');
      field.unbind("change");
      $('#' + this.property + '-slider').slider("destroy");
    },
    enable : function () {
      this.initialize(this.property, this.settings);
    }
  };

  Dtc.Toolbar.DtcWidthHeightWidget = Class.create();
  Dtc.Toolbar.DtcWidthHeightWidget.prototype = {
    initialize : function (property, settings) {
      this.property = property;
      this.settings = settings;
      this.widget = new Dtc.Toolbar.DtcSliderWidget(property, settings);
      this.fieldSelector = '#' + property + '-field';
      this.sliderSelector = '#' + property + '-slider';
      this.stateFlagSelector = '#' + property + '-state-flag';
      this.defaultValues = {};
      var self = this;

      this.setDynamic();

      $(this.stateFlagSelector).click(function (event) {
        event.preventDefault();
        (self.state == 'dynamic') ? self.setStatic() : self.setDynamic();
      });
    },
    setValue : function (value) {
      this.widget.setValue(value);
    },
    setDynamic : function () {
      var current = Dtc.editor.currentlyEditing[Dtc.editor.currentlySelected];
      this.state = 'dynamic';
      var self = this;
      $(this.stateFlagSelector).text('dynamic');
      $(this.fieldSelector).attr('disabled', 'true');
      $(this.sliderSelector).slider('option', 'disabled', true);
      if (Dtc.editor.currentlyEditing['tag'] != '') {
        this.widget.showPreview(this.property, self.defaultValues[current]);
        this.setValue(this.defaultValues[current]);
        Dtc.config.popCss(current, this.property);
      }

    },
    setStatic : function () {
      // Store the default value.
      var current = Dtc.editor.currentlyEditing[Dtc.editor.currentlySelected];
      this.defaultValues[current] = $(this.fieldSelector).val();
      this.state = 'static';
      $(this.stateFlagSelector).text('static');
      $(this.fieldSelector).attr('disabled', false);
      $(this.sliderSelector).slider('option', 'disabled', false);
      Dtc.config.pushCss(current, this.property, $(this.fieldSelector).val());
    },
    disable : function () {
      $(this.stateFlagSelector).unbind("click");
      this.widget.disable();
    },
    enable : function () {
      this.initialize(this.property, this.settings);
    }
  };

  /**
   * Handles a toolbar's slider.
   *
   * @param property
   *          The CSS property handled by this widget.
   * @param settings
   *          Optional. The settings of the slider.
   */
  Dtc.Toolbar.DtcSliderWidget = Class.create();
  Dtc.Toolbar.DtcSliderWidget.prototype = {
    initialize : function (property, settings) {
      if (settings == null) {
        settings = {
          value : 0,
          min : 0,
          max : 30,
          step : 1
        };
      }
      var w = this;
      this.property = property;
      var field = $('#' + this.property + '-field');
      field.change(function () {
        w.setValue($(this).val());
        Dtc.showPreview(w.property, $(this).val() + "px");
      });
      this.settings = settings;
      $('#' + this.property + '-slider').slider({
        range : "min",
        value : w.settings.value,
        min : w.settings.min,
        max : w.settings.max,
        step : w.settings.step,
        slide : function (event, ui) {
          if (Dtc.editor.currentlyEditing['tag'] == '') {
            $('#no-selected-item').dialog('open');
            event.stopImmediatePropagation();
            return false;
          }
          if (field[0].tagName.toLowerCase() != 'input') {
            field.text(ui.value);
          }
          else {
            field.val(ui.value);
          }
          // Dtc.showPreview(w.property,ui.value + "px");
          w.showPreview(w.property, ui.value);
        }
      });
    },
    showPreview : function (property, value) {
      Dtc.showPreview(property, value + "px");
    },
    setValue : function (value) {
      value = "" + value;
      value = value.replace("px", "").replace("normal", "0");
      var field = $('#' + this.property + '-field');
      if (field[0].tagName.toLowerCase() != 'input') {
        field.text(Math.floor(value));
      }
      else {
        field.val(Math.floor(value));
      }
      if (!$.browser.msie && !$.browser.opera) {
        $('#' + this.property + '-slider').slider("value", Math.floor(value));
      }
    },
    reset : function () {
      this.setValue(this.settings.value);
    },
    disable : function () {
      var field = $('#' + this.property + '-field');
      field.unbind("change");
      $('#' + this.property + '-slider').slider("destroy");
    },
    enable : function () {
      this.initialize(this.property, this.settings);
    }
  };

  Dtc.Toolbar.DtcSliderLockWidget = Class.create();
  Dtc.Toolbar.DtcSliderLockWidget.prototype = {
    initialize : function () {
      this.args = this.initialize.arguments;
      // The first argument of the function is the id of the control
      // checkbox, others are the widgets.
      this.sliderWidgets = this.initialize.arguments;
      // Store the id of the checkbox.
      this.checkboxName = this.sliderWidgets[0];
      // Store the object.
      var self = this;
      // Bind the change and the slide event to the sliders.
      for ( var i = 1; i < this.sliderWidgets.length; i++) {
        $('#' + this.sliderWidgets[i].property + '-slider').bind('slidestop',
            function (event, ui) {
              if ($('[name="' + self.checkboxName + '"]').is(':checked')) {
                self.moveTogether(event, ui);
              }
            });
      }
    },
    moveTogether : function (event, ui) {
      for ( var i = 1; i < this.sliderWidgets.length; i++) {
        this.sliderWidgets[i].setValue(ui.value);
        if (this.sliderWidgets[i].property.search("-radius") > 0) {
          this.sliderWidgets[i].showBorderRadiusPreview(ui.value);
        }
        else {
          Dtc.showPreview(this.sliderWidgets[i].property, ui.value + "px");
          $('#' + this.sliderWidgets[i].property + '-slider').trigger(
              "slidestart", ui);
        }
      }
    },
    disable : function () {
      for ( var i = 1; i < this.sliderWidgets.length; i++) {
        $('#' + this.sliderWidgets[i].property + '-slider').unbind('slide');
      }
    },
    enable : function () {
      this.initialize.apply(this, this.args);
    }
  };

  Dtc.Toolbar.DtcBorderStyleLock = Class.create();
  Dtc.Toolbar.DtcBorderStyleLock.prototype = {
    initialize : function () {
      this.args = this.initialize.arguments;
      this.widgets = this.initialize.arguments;
      this.checkboxSelector = '#' + this.widgets[0];
      var self = this;
      this.value = 'none';
      for ( var i = 1; i < this.widgets.length; i++) {
        var selector = '#' + this.widgets[i].property + '-field';
        $(selector).bind('change', function (event) {
          self.changeCallback(event);
        });
      }
    },
    changeCallback : function (event) {
      var self = this;
      if ($(self.checkboxSelector).is(':checked')) {
        this.value = $(event.target).val();
        for ( var i = 1; i < this.widgets.length; i++) {
          var selector = '#' + self.widgets[i].property + '-field';
          $(selector).unbind('change');
        }
        this.action(event);
      }
    },
    action : function (event) {
      var self = this;
      for ( var j = 1; j < self.widgets.length; j++) {
        self.widgets[j].setValue(self.value);
        self.widgets[j].showBorderStylePreview(self.value);
        $('#' + self.widgets[j].property + '-field').bind('change', function () {
          self.changeCallback(event);
        });
      }
    },
    disable : function () {
      for ( var i = 1; i < this.widgets.length; i++) {
        var selector = '#' + this.widgets[i].property + '-field';
        $(selector).unbind('change');
      }
    },
    enable : function () {
      this.initialize.apply(this, this.args);
    }
  };

  /**
   * Handles the border-width CSS property.
   */
  Dtc.Toolbar.DtcBorderWidth = Class.create();
  Dtc.Toolbar.DtcBorderWidth.prototype = {
    initialize : function (property, settings) {
      this.property = property;
      this.settings = settings;
      this.widget = new Dtc.Toolbar.DtcSliderWidget(this.property,
          this.settings);
      this.setValue = this.widget.setValue;
      this.reset = this.widget.reset;
      $('#' + property + '-slider').bind("slidestart", function (event, ui) {
        var styleProperty = property.replace("width", "style");
        var colorProperty = property.replace("width", "color");
        var styleValue = $("#" + styleProperty + "-field").val();
        var color = $("input[name='" + colorProperty + "']").val();
        Dtc.showPreview(property, ui.value + "px");
        Dtc.showPreview(styleProperty, styleValue);
        Dtc.showPreview(colorProperty, color);
      });
    },
    disable : function () {
      $('#' + this.property + '-slider').unbind("slidestart");
      this.widget.disable();
    },
    enable : function () {
      this.initialize(this.property, this.settings);
    }
  };
  /**
   * Handles the text-decoration CSS property.
   */
  Dtc.Toolbar.DtcTextDecoration = Class.create();
  Dtc.Toolbar.DtcTextDecoration.prototype = {
    initialize : function () {
      $('input[value*="text-decoration"]').click(function () {
        var values = new Array();
        $('input.[value*="text-decoration"]:checked').each(function () {
          values.push(this.value.replace('text-decoration-', ''));
        });
        Dtc.showPreview('text-decoration', values.join(' '));
      });
    },
    setValue : function (value) {
      $('input[value*="text-decoration"]').attr('checked', false);
      if (value != 'none') {
        var v = value.split(' ');
        for (var i = 0; i < v.length; i++) {
          $('input[value="text-decoration-' + v[i] + '"]').attr('checked', true);
        }
      }
      $('input[value*="text-decoration"]').button("refresh");
    },
    reset : function () {
      $('input[value*="text-decoration"]').attr('checked', false);
      $('input[value*="text-decoration"]').button('refresh');
    },
    disable : function () {
      $('input[name="text-decoration"]').unbind("click");
    },
    enable : function () {
      this.initialize();
    }
  };
  /**
   * This widget will log to the console.
   */
  Dtc.Toolbar.DtcConsoleLoggerWidget = Class.create();
  Dtc.Toolbar.DtcConsoleLoggerWidget.prototype = {
    initialize : function () {
      if ($.browser.opera && opera.postError) {
        this.logger = opera.postError;
      }
      else if (console.log) {
        this.logger = console.log;
      }
      else {
        this.logger = alert;
      }
    },
    setValue : function (value) {
      var text = "[" + property + "]:" + value;
      this.logger(text);
    },
    reset : function () {
    },
    disable : function () {
    },
    enable : function () {
    }

  };
  /**
   * Background position FF: px => px em => px % => % left, center, top, etc => %
   * none => 0% 0%
   *
   * Chrome: px => px em => em % => % left, center, top, etc => % none => 0% 0%
   *
   * Opera: px => px em => em % => % left, center, top, etc => % none => none
   *
   * IE8: px => px em => px % => px left, center, top, etc => left, center, top,
   * etc none => 0px 0px
   *
   * @param axis
   *          x or y position of background (null will be both of them)
   */
  Dtc.Toolbar.DtcBackgroundPosition = Class.create();
  Dtc.Toolbar.DtcBackgroundPosition.prototype = {
    initialize : function (axis) {
      this.axis = axis;
      $(".background-position").change(
          function () {
            var x = $("#background-position-x-value").val()
                + $("#background-position-x-unit").val();
            var y = $("#background-position-y-value").val()
                + $("#background-position-y-unit").val();
            Dtc.showPreview("background-position", x + " " + y);
          });
    },
    parseValue : function (value, ax) {
      var unit;
      if (value.indexOf("px") != -1) {
        unit = "px";
      }
      else if (value.indexOf("%") != -1) {
        unit = "%";
      }
      else if (value.indexOf("em") !== -1) {
        unit = "em";
      }
      else {
        // Handle keywords (left, center, top, etc.).
        unit = "%";
        if (value == "top" || value == "left") {
          value = "0%";
        }
        else if (value == "center") {
          value = "50%";
        }
        else if (value == "right" || value == "bottom") {
          value = "100%";
        }
      }
      value = value.replace(unit, "");
      $("#background-position-" + ax + "-unit").val(unit);
      $("#background-position-" + ax + "-value").val(value);
    },
    setValue : function (value) {
      if (value == "none") {
        value = "0% 0%";
      }

      value = value.split(" ");
      if (value.length == 1) {
        this.parseValue(value[0], this.axis);
      }
      else {
        this.parseValue(value[0], "x");
        this.parseValue(value[1], "y");
      }
    },
    reset : function () {
      this.setValue("0% 0%");
    },
    disable : function () {
      $(".background-position").unbind("change");
    },
    enable : function () {
      this.initialize(this.axis);
    }
  };
  /**
   * Handles the display CSS propertry.
   */
  Dtc.Toolbar.DtcDisplay = Class.create();
  Dtc.Toolbar.DtcDisplay.prototype = {
    initialize : function () {
      this.hiddenElements = {};

      var self = this;
      $("#display-field")
          .change(
              function (e) {
                var current = Dtc.editor.currentlyEditing[Dtc.editor.currentlySelected];
                var previous = $("#display-field option.previous-selected")
                    .text();
                $("#display-field option.previous-selected")
                    .removeAttr("class");
                $("#display-field option:selected").attr("class",
                    "previous-selected");
                if ($(this).val() == "none") {
                  // Add element.
                  self.hiddenElements[current] = previous;
                }
                else if (previous == "none") {
                  // Remove element.
                  delete self.hiddenElements[current];
                }
                Dtc.showPreview("display", $(this).val());
              });
      $("#restore-hidden").click(function () {
        for ( var elem in self.hiddenElements) {
          Dtc.css(elem, "display", self.hiddenElements[elem]);
          Dtc.config.pushCss(elem, "display", self.hiddenElements[elem]);
        }
        self.hiddenElements = {};
      });
    },
    setValue : function (value) {
      $("#display-field option.previous-selected").removeAttr("class");
      $("#display-field").val(value);
      $("#display-field option:selected").attr("class", "previous-selected");
    },
    reset : function () {
      this.setValue("inline");
    },
    disable : function () {
      $("#display-field").unbind("change");
      $("#restore-hidden").unbind("click");
    },
    enable : function () {
      this.initialize();
    }
  };
  Dtc.toolbar = new Dtc.Toolbar();

  /**
   * [END] Toolbar
   */

  /**
   * Loads CSS properties.
   *
   * @param cssProps
   *          {Array} Array of properties we want to load back. If null we will
   *          load back everything.
   */
  Dtc.loadCssProps = function (cssProps) {
    var node = $(Dtc.editor.currentlyEditing['id']);
    var value;

    if (!cssProps) {
      cssProps = [ "background-color", "text-align", "font-style", "font-size",
          "font-weight", "font-family", "color", "text-decoration",
          "text-transform", "direction", "line-height", "word-spacing",
          "letter-spacing", "text-indent", "vertical-align", "padding-left",
          "padding-right", "margin-left", "margin-right", "padding-top",
          "padding-bottom", "margin-top", "margin-bottom", "border-left-color",
          "border-left-style", "border-left-width", "border-right-color",
          "border-right-style", "border-right-width", "border-top-color",
          "border-top-style", "border-top-width", "border-bottom-color",
          "border-bottom-style", "border-bottom-width", "background-image",
          "background-attachment", "background-repeat", "float", "display",
          "clear" ];
      if (!$.browser.msie) {
        cssProps.push("width", "height", "background-position");
      }
      else {
        cssProps.push("background-position-x", "background-position-y");
      }
    }
    for ( var i in cssProps) {
      value = node.css(cssProps[i]);
      Dtc.toolbar.get(cssProps[i]).setValue(value);
    }
    if (!$.browser.msie) {
      for ( var i in Dtc.borderRadius) {
        simple = i;
        moz = Dtc.borderRadius[i]['moz'];
        webkit = Dtc.borderRadius[i]['webkit'];

        value = node.css(simple);
        if (value != '') {
          Dtc.toolbar.get(i).setValue(value);
          continue;
        }
        value = node.css(webkit);
        if (value != '') {
          Dtc.toolbar.get(i).setValue(value);
          continue;
        }
        else {
          value = node.css(moz);
          Dtc.toolbar.get(i).setValue(value);
        }
      }
    }
  };

  Dtc.BubbleInfo = Class.create();
  Dtc.BubbleInfo.prototype = {
    initialize : function () {
    },
    enable : function () {
      var path = (Dtc.editorOnly) ? 'dtc/editor/get-help' : 'dtc/get-help';
      $('.bubble-trigger').each(function () {
        var parameter = $(this).attr('name');
        var url = Drupal.settings.basePath + '?q=' + path + '/' + parameter;
        $(this).attr("rel", url);
        $(this).cluetip({
          cluezIndex : 100000,
          positionBy : 'mouse',
          showTitle : false
        });
      });
      $("#cluetip").addClass("no-hover");
      $("#cluetip").find("*").addClass("no-hover");
      $("#cluetip-waitimage").addClass("no-hover");
      $("#cluetip-waitimage").find("*").addClass("no-hover");
    },
    disable : function () {
      $('.bubble-trigger').each(function () {
        $(this).cluetip('destroy');
      });
    }
  };
  Dtc.bubbleInfo = new Dtc.BubbleInfo();

  Dtc.GenerateDialog = Class.create();
  Dtc.GenerateDialog.prototype = {
    initialize : function () {
      this.name = "#edit-name",
        this.full_name = "#edit-full-name",
        this.description = "#edit-description",
        this.allFields = [ this.name, this.full_name, this.description ],
        this.tips = ".validateTips";
    },
    createForm : function () {
      var self = this;
      var form = Drupal.theme('dtcGenerateDialog');
      $("body").append(form);
      $("#generate-dialog-form").dialog(
        {
          autoOpen : false,
          height : 435,
          width : 360,
          modal : true,
          resizable : false,
          buttons : {
            'Save' : function () {
              var bValid = true;
              $.each(self.allFields, function (idx, elem) {
                $(elem).removeClass('ui-state-error');
              });
              if (Dtc.editorOnly) {
                bValid = bValid
                    && self.checkLength($(self.name), "CSS file's name",
                        1, null);
                bValid = bValid
                    && self
                        .checkRegexp(
                            $(self.name),
                            /([0-9a-z_])+(\.css)?$/i,
                            Drupal
                                .t("CSS file's name may consist of a-z, 0-9, underscores and a .css extension."));
              }
              else {
                bValid = bValid
                    && self.checkLength($(self.name), "folder's name", 1,
                        null);
                bValid = bValid
                    && self
                        .checkRegexp(
                            $(self.name),
                            /([0-9a-z_])+$/i,
                            Drupal
                                .t("Folder's name may consist of a-z, 0-9, underscores."));
              }
              if (bValid) {
                Dtc.config.pushGeneral("name", $(self.name).val());
                if (!Dtc.editorOnly && $(self.full_name).val().length != 0)
                  Dtc.config.pushGeneral("full_name", $(self.full_name).val());

                if (!Dtc.editorOnly && $(self.description).val().length != 0)
                  Dtc.config.pushGeneral("description", $(self.description).val());

                Dtc.config.save();
                $(this).dialog('close');
                self.destroyForm();
              }
            },
            Cancel : function () {
              $(this).dialog('close');
              self.destroyForm();
            }
          },
          close : function () {
            $.each(self.allFields, function (idx, elem) {
              $(elem).val('').removeClass('ui-state-error');
            });
          }
        });
    },
    destroyForm : function () {
      $("#generate-dialog-form").dialog("destroy");
      $("#generate-dialog-form").remove();
    },
    enable : function () {
      var self = this;
      // Generate button's action.
      $('#generate').click(function (event) {
        event.preventDefault();
        self.createForm();
        $("#generate-dialog-form").dialog('open');
      });
    },
    /**
     * Show error messages
     */
    updateTips : function (t) {
      var self = this;
      $(self.tips).text(t).addClass('ui-state-highlight');
      setTimeout(function () {
        $(self.tips).removeClass('ui-state-highlight', 1500);
      }, 500);
    },
    /**
     * Checks the length of an object's value and shows an error message if
     * necessary.
     */
    checkLength : function (o, n, min, max) {
      if (max == null) {
        if (o.val().length < min) {
          o.addClass('ui-state-error');
          var params = {
            '@value' : n,
            '@min' : (min - 1)
          };
          this.updateTips(Drupal.t(
              "Length of @value must be greater than @min.", params));
          return false;
        }
        else
          return true;
      }
      else if (o.val().length > max || o.val().length < min) {
        o.addClass('ui-state-error');
        var params = {
          '@value' : n,
          '@min' : (min),
          '@max' : max
        };
        this.updateTips(Drupal.t(
            "Length of @value must be between @min and @max.", params));
        return false;
      }
      else {
        return true;
      }

    },
    /**
     * Checks a regexp on an object's value.
     */
    checkRegexp : function (o, regexp, n) {
      if (!(regexp.test(o.val()))) {
        o.addClass('ui-state-error');
        this.updateTips(n);
        return false;
      }
      else {
        return true;
      }

    }
  };
  Dtc.generateDialog = new Dtc.GenerateDialog();

  Drupal.theme.prototype.dtcGenerateDialog = function () {
    return '<div id="generate-dialog-form" title="' + Drupal.t('Save your theme')  + '">'
      + '<p class="validateTips"></p>'
      + '<form id="dtc-generate-theme-form">'
      + '<div><fieldset><div><label for="edit-name">'
      + Drupal.t('Name of your CSS file:')
      + '<span class="form-required" title="' + Drupal.t('Required') + '">*</span>'
      + '</label>'
      + '<input id="edit-name" class="form-text required text" type="text" value="" size="30" name="name" maxlength="128" />'
      + '</div></fieldset></div></form></div>';
  };

  Drupal.theme.prototype.dtcHeaderButtons = function () {
    return '<div id="buttons">'
      + '<button id="turn-off">' + Drupal.t('Turn off') + '</button>'
      + '<div id="toolbar-opacity-slider-wrapper" title="Set the opacity of the toolbar"><div id="toolbar-opacity-slider"></div></div>'
      + '<button id="undo">' + Drupal.t('Undo') + '</button>'
      + '<button id="redo">' + Drupal.t('Redo') + '</button>'
      + '<button id="clear-all">' + Drupal.t('Clear all') + '</button>'
      + '<button id="generate">' + Drupal.t('Generate') + '</button>'
      + '<button id="dock-toolbar">' + Drupal.t('Dock toolbar') + '</button>' 
      + '<button id="minimize-toolbar">' + Drupal.t('Minimize') + '</button>'
      + '</div>';
  };

  Drupal.theme.prototype.dtcClearDialog = function () {
    return '<div id="clear-dialog-confirm" title="' + Drupal.t('Delete all modifications?')  + '">'
      + '<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'
      + Drupal.t('Your modifications cannot be recovered. Are you sure?')
      + '</p></div>';
  };
})(jQuery);
