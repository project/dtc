// $Id$

/**
 * @file
 * Core classes for Dtc Editor.
 *
 * Copyright 2010 by Department of Software Engineering, University of Szeged, Hungary
 */

/**
 * Helper for class creating in Jacascript.
 */
var Class = {
  create : function () {
    return function () {
      this.initialize.apply(this, arguments);
    };
  }
};
var Dtc = Dtc || {};

(function ($) {
  Dtc.editorOnly = true;
  /**
   * We store the modifications in this object.
   */
  Dtc.Config = Class.create();
  Dtc.Config.prototype = {
    initialize : function () {
      this.modified = false;
      this.data = {
        css : {},
        regions : {},
        general : {}
      };
    },
    /**
     * Saves a CSS property's value changed at the element selected by the
     * selector.
     */
    pushCss : function (selector, prop, val) {
      this.modified = true;
      if (!this.data.css[selector]) {
        this.data.css[selector] = {};
      }

      this.data.css[selector][prop] = val;
    },
    /**
     * Clears a CSS property's value of the element selected by the selector.
     */
    popCss : function (selector, prop) {
      if (this.data.css[selector] && this.data.css[selector][prop]) {
        delete this.data.css[selector][prop];
      }
    },
    /**
     * Saves the selected structure of the page.
     */
    pushRegions : function (regions, callback) {
      this.modified = true;
      this.data.regions = regions;
      if (callback) {
        this.data.regionsCallback = callback;
      }
    },
    /**
     * Saves a general information.
     */
    pushGeneral : function (key, value) {
      this.data.general[key] = value;
    },
    /**
     * Sends our config object back to server.
     */
    save : function () {
      var dataString = $.toJSON(Dtc.config.data);
      var path;
      if (Dtc.editorOnly) {
        $.post(Drupal.settings.basePath + "dtc/editor/save", {
          dtcConfig : dataString,
          theme: Drupal.settings.dtc.theme
        });
      }
      else {
        $.post(Drupal.settings.basePath + "dtc/save", {
          dtcConfig : dataString
        }, function (data) {
          path = $.evalJSON(data);
          window.location.href = path;
        });
      }
    },
    store : function () {
      var dataString = $.toJSON(Dtc.config.data);
      if (Dtc.editorOnly) {
        $.ajax({
          async : false,
          data : {
            dtcConfig : dataString,
            theme : Drupal.settings.dtc.theme
          },
          type : 'POST',
          url : Drupal.settings.basePath + "dtc/editor/store"
        });
      }
      else {
        $.cookie(theme + "-cookie", datastring);
      }
    },
    restore : function () {
      $.ajax({
        async : false,
        data : {
          theme : Drupal.settings.dtc.theme
        },
        dataType : 'json',
        type : 'POST',
        url : Drupal.settings.basePath + "dtc/editor/restore",
        success : function (data) {
          if (data) {
            Dtc.config.data = data;
            Dtc.config.modified = true;
          }
        }
      });
    }
  };
  Dtc.config = new Dtc.Config();

  /**
   * Inserts a CSS property into our style tag.
   *
   * @param selector
   *          The CSS selector
   * @param prop
   *          The name of the property.
   * @param val
   *          The value of the property.
   */
  Dtc.css = function (selector, prop, val) {
    // Create our style tag if it is not present.
    if ($("style[dtc='dtc']").length == 0) {
      $("head").append("<style type='text/css' dtc='dtc'></style>");
    }

    // Look for previous style definition.
    if (selector.indexOf("#") == -1 && selector.indexOf(".") == -1 && selector.indexOf(" ") == -1) {
      var status = /:.+/.exec(selector);
      if (status) {
        status = status[0];
        selector = selector.replace(status, "");
      }
      selector = selector + ".dtc-editable";
      if (status) {
        selector = selector + status;
      }
    }

    var styleContent = $("style[dtc='dtc']").text();
    var selectorPattern = new RegExp(selector + " {[^}]*}\n\n", "");
    var m = styleContent.match(selectorPattern);
    if (m != null) {
      // We got it.
      var style = '';
      // Modify the selected style definition if prop is not empty.
      if (prop.length > 0) {
        style = m[0];
        var propPattern = new RegExp("\t" + prop + ".*\n", "");
        // Look for the given property.
        var existingProp = style.match(propPattern);
        if (existingProp != null) {
          // Replace it with the new one or delete it if val is empty.
          if (val.length > 0) {
            style = style
                .replace(propPattern, "\t" + prop + ": " + val + ";\n");
          }
          else {
            style = style.replace(propPattern, "");
          }
        }
        else {
          // Add it as a new property if val is not empty.
          if (val.length > 0) {
            style = style.replace("}\n\n", "\t" + prop + ": " + val
                + ";\n}\n\n");
          }
        }
      }
      // Replace the old style definition with the new one.
      // The style defintion will be removed is prop was empty.
      styleContent = styleContent.replace(selectorPattern, style);
      $("style[dtc='dtc']").text(styleContent);
    }
    else {
      // Add it as a new style definition if prop and val is not empty.
      if (prop.length > 0 && val.length > 0) {
        $("style[dtc='dtc']").append(
            selector + " {\n\t" + prop + ": " + val + ";\n" + "}\n\n");
      }
    }
  };
})(jQuery);
