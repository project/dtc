// $Id$

/**
 * @file
 * We add this JS file after everyhting has been loaded.
 * Only this file contains $(document).ready()
 *
 * Copyright 2010 by Department of Software Engineering, University of Szeged, Hungary
 */

var Dtc = Dtc || {};

(function ($) {
  Dtc.enable = function () {
    Dtc.toolbar.enable();
    Dtc.editor.enable();
    Dtc.generateDialog.enable();
    Dtc.reload.enable();
    Dtc.undoRedo.enable();
    Dtc.bubbleInfo.enable();
    Dtc.reload.reloadTheTheme();
  };
  $(document).ready(function () {
    Dtc.enable();
  });
})(jQuery);
