// $Id$

/**
 * @file
 * This file contains classes needed for editing an element on the site.
 *
 * Copyright 2010 by Department of Software Engineering, University of Szeged, Hungary
 */

var Dtc = Dtc || {};

(function ($) {
  /**
   * Applies changes in the css and save it to our global object.
   *
   * @param prop
   *          Name of the property.
   * @param val
   *          Value we want to set.
   */
  Dtc.showPreview = function (prop, val) {
    var selector = Dtc.editor.currentlyEditing[Dtc.editor.currentlySelected];
    var status = $("#status-select-menu a[aria-selected='true']").text();
    if (status != "none") {
      selector += status;
    }
    Dtc.undoRedo.addCommand(new Dtc.StyleCommand(selector, prop, val));
    Dtc.toolbar.resetStyle();
  };

  Dtc.Editor = Class.create();
  Dtc.Editor.prototype = {
    initialize : function () {
      this.currentlyEditing = {};
      this.currentlyEditing['id'] = '';
      this.currentlyEditing['tag'] = '';
      this.currentlyEditing['class'] = '';
      this.currentlySelected = 'id';
    },
    /**
     * Gets the tag name of an event's target.
     *
     * @param target
     *          Target member of the event object.
     * @return The name of the tag.
     */
    getTagName : function (target) {
      return target.tagName.toLowerCase();
    },
    /**
     * Gets the class of an event's target.
     *
     * @param target
     *          Target member of the event object.
     * @return Returns the class of the target.
     */
    getClass : function (target) {
      var cssClass = $.trim(target.className.replace("hovering", ""));
      if (cssClass != "") {
        cssClass = "."
            + cssClass.split(" ").join(".").replace("...", "")
                .replace("..", "");
        cssClass = cssClass.replace(".dtc-editable", "");
      }
      if (cssClass == "") {
        cssClass = "undefined";
      }
      return cssClass;
    },
    /**
     * Gets the id of an event's target.
     *
     * @param target
     *          Target member of the event object.
     * @return Returns the id of the target.
     */
    getId : function (target) {
      var id;
      if (target.id != "") {
        id = "#" + target.id;
      }
      else {
        id = "undefined";
      }
      return id;
    },
    /**
     * Gets an identifier of an event's target. It will be the element's id.
     *
     * @param target
     *          Target member of the event object.
     * @return
     */
    getIdentifier : function (target) {
      var id;
      var tagName = this.getTagName(target);
      if (tagName == 'body') {
        return tagName;
      }

      id = this.getId(target);
      if (id != "undefined") {
        return id;
      }

      id = this.getClass(target);
      if (id != "undefined") {
        return id;
      }

      return tagName;
    },

    /**
     * Gets the route to the event's target. It will be a route to the element.
     *
     * @param target
     *          Target member of the event object.
     * @return Returns a route to the target element.
     */
    getRoute : function (target) {
      var route = this.getIdentifier(target);
      if (target.id != "") {
        return route;
      }

      var parent = target.parentNode;
      while (parent.tagName.toLowerCase() != "html") {
        route = this.getIdentifier(parent) + " " + route;
        if (parent.id != "") {
          break;
        }

        parent = parent.parentNode;
      }
      return route;
    },
    doClick : function (route, tag, cssClass) {
      $(".selector").button("enable");

      Dtc.editor.currentlyEditing['id'] = route;
      Dtc.editor.currentlyEditing['tag'] = tag;
      if (cssClass != "undefined") {
        Dtc.editor.currentlyEditing['class'] = cssClass;
        $("#class-selector").selectmenu("destroy");
        $("#class-selector").empty();
        $("#class-selector").append(
            '<option value="dtc-no-class">' + Drupal.t('Class...')
                + '</option>');
        $.each(cssClass.split('.'), function (idx, elem) {
          if (elem.length > 0) {
            $("#class-selector").append(
                '<option value="' + elem + '">' + elem + '</option>');
          }
        });
        $("#class-selector").selectmenu({
          style : 'dropdown',
          width : 200
        });
      }
      else {
        $("#class-selector").selectmenu("destroy");
        $("#class-selector").empty().append(
            '<option value="dtc-no-class">' + Drupal.t('Class...')
                + '</option>');
        $("#class-selector").selectmenu({
          style : 'dropdown',
          width : 200
        });
        $("#class-selector").selectmenu("disable");
        Dtc.editor.currentlyEditing['class'] = "";
      }
      $("#status-select").selectmenu("destroy");
      $("#status-select").html(
          '<option type="radio" value="none">none</option>'
              + '<option type="radio" value="hover">:hover</option>');
      if (tag == 'a') {
        $("#status-select").append(
            '<option type="radio" value="link">:link</option>').append(
            '<option type="radio" value="visited">:visited</option>').append(
            '<option type="radio" value="active">:active</option>');
      }
      $("#status-select").selectmenu({
        style : 'dropdown',
        width : 80
      });
      $("#tag-selector span").text(
          Drupal.t('All') + ' <' + Dtc.editor.currentlyEditing['tag'] + '>');
      Dtc.path.createPath(route);
    },
    /**
     * Adds hover effect for all elements on a page.
     */
    hoverOn : function () {
      var notSelectList = "html, head, meta, script, link, style, .no-hover";
      if (Dtc.editorOnly) {
        notSelectList += ", #dtc-switcher";
      }
      var selector = "*:not(" + notSelectList + ")";
      var parent = $("#dtc-toolbar").parent();
      if (parent) {
        $(parent).addClass("no-hover");
        $(parent).find("*").addClass("no-hover");
      }
      parent = $("#no-selected-item").parent();
      if (parent) {
        $(parent).addClass("no-hover");
        $(parent).find("*").addClass("no-hover");
      }
      parent = $("#generate-dialog-form");
      if (parent) {
        $(parent).addClass("no-hover");
        $(parent).find("*").addClass("no-hover");
      }

      $("#class-selector-menu").addClass("no-hover").find("*").addClass(
          "no-hover");
      $("#class-selector-button").addClass("no-hover").find("*").addClass(
          "no-hover");

      $(selector).addClass("dtc-editable");

      $(selector).hover(function (e) {
        e.stopPropagation();
        if ($(this)[0].tagName.toLowerCase() != e.target.tagName
            .toLowerCase()) {
          return false;
        }

        Dtc.editor.hoverOff();
        if (!$(this).hasClass("selected")) {
          Dtc.fade.savedBackground = $(this).css("background-color");
          $(this).css("background-color", "#d8f1f8");
        }
        Dtc.toolbar.hovered = $(this);
        return false;
      },
      function (e) {
        e.stopPropagation();
        if ($(this)[0].tagName.toLowerCase() != e.target.tagName
            .toLowerCase()) {
          return false;
        }

        Dtc.editor.hoverOff();
        return false;
      });
      $(selector).click(function (e) {
        e.stopPropagation();
        if ($(this)[0].tagName.toLowerCase() != e.target.tagName
            .toLowerCase()) {
          return true;
        }
        Dtc.toolbar.hideToolbarError();
        if (Dtc.fade.animating) {
          return false;
        }

        $("#jump-link").css("display", "none");
        var target = e.currentTarget;
        if ($.browser.msie) {
          target = e.target;
        }

        var route = Dtc.editor.getRoute(target);
        var tag = Dtc.editor.getTagName(target);
        var cssClass = Dtc.editor.getClass(target);
        Dtc.editor.doClick(route, tag, cssClass);
        Dtc.undoRedo.newGroup();
        if (Dtc.editorOnly) {
          if (tag == 'a') {
            if ($("#jump-link").length == 0) {
              $("body").append("<div id='jump-link'></div>");
            }
            $("#jump-link").html(
                "<a href='" + this.href + "'>"
                    + Drupal.t("Follow this link") + "</a>");
            $("#jump-link").css("top", e.pageY).css("left", e.pageX).css(
                "display", "block");
          }

        }
        $("#id-selector").click();

        Dtc.toolbar.moveToolbar(e);
        return false;
      });
    },
    /**
     * Turns off hovering effect on the hovered element.
     */
    hoverOff : function () {
      if (Dtc.toolbar.hovered) {
        if (!Dtc.toolbar.hovered.hasClass("selected")) {
          Dtc.toolbar.hovered.css("background-color", "");
        }

        Dtc.toolbar.hovered = null;
      }
    },
    /**
     * Adds functionality to the active element selectors.
     */
    handleCurrentlySelected : function () {
      $("#id-selector").click(function (e) {
        e.stopPropagation();
        Dtc.editor.currentlySelected = "id";
        $("#tag-selector").removeClass("dtc-ui-state-active");
        $("#class-selector-button").removeClass("dtc-ui-state-active");
        $("#class-selector").selectmenu("value", "dtc-no-class");
        $(this).addClass("dtc-ui-state-active");
        $("#apply-button").button("disable");

        if (Dtc.fade.animating) {
          return false;
        }

        Dtc.fade.doFade(Dtc.editor.currentlyEditing['id']);
      });
      $("#class-selector").change(function (e) {
        e.stopPropagation();
        $(".selector").removeClass("dtc-ui-state-active");
        $("#class-selector-button").addClass("dtc-ui-state-active");
        if ($(this).val() == "dtc-no-class") {
          $("#id-selector").click();
        }
        else {
          Dtc.editor.currentlyEditing['class'] = "." + $(this).val();
          Dtc.editor.currentlySelected = 'class';
        }
        if (Dtc.undoRedo.modified) {
          $("#apply-button").button("enable");
        }
        else {
          $("#apply-button").button("disable");
        }
        if (Dtc.fade.animating) {
          return false;
        }

        Dtc.fade.doFade(Dtc.editor.currentlyEditing['class']);
      });

      $("#tag-selector").click(
          function (e) {
            e.stopPropagation();
            Dtc.editor.currentlySelected = "tag";
            $("#id-selector").removeClass("dtc-ui-state-active");
            $("#class-selector-button").removeClass("dtc-ui-state-active");
            $(this).addClass("dtc-ui-state-active");

            if (Dtc.undoRedo.modified) {
              $("#apply-button").button("enable");
            }
            else {
              $("#apply-button").button("disable");
            }

            if (Dtc.fade.animating) {
              return false;
            }

            Dtc.fade.doFade(Dtc.editor.currentlyEditing['tag']
                + ":not(.no-hover)");
          });
    },
    setCurrently : function (currentlyEditing, currentlySelected) {
      this.currentlySelected = currentlySelected;
      this.doClick(currentlyEditing['id'], currentlyEditing['tag'],
          currentlyEditing['class']);
      $(".selector").removeClass("dtc-ui-state-active");
      switch (currentlySelected) {
      case "id":
      case "tag":
        $("#" + currentlySelected + "-selector")
            .addClass("dtc-ui-state-active");
        break;
      case "class":
        var class = currentlyEditing['class'].replace(".", "");
        $("#class-selector option:selected").removeAttr("selected");
        $("#class-selector option:contains('" + class + "')").attr("selected",
            true);
        $("#class-selector").selectmenu("destroy");
        $("#class-selector").selectmenu({
          style : 'dropdown',
          width : 200
        });
        $("#class-selector-button").addClass("dtc-ui-state-active");
        break;
      }
    },
    enable : function () {
      this.hoverOn();
      this.handleCurrentlySelected();
    }
  };
  Dtc.editor = new Dtc.Editor();

  /**
   * Represents the path to the selected element.
   */
  Dtc.Path = Class.create();
  Dtc.Path.prototype = {
    initialize : function () {
    },
    pathToActive : function () {
      var n = $("#dtc-path button.route");
      var r = new Array();
      for ( var i = 0; i < n.length; i++) {
        var node = $(n[i]).find("span");
        if (node.text().indexOf("#") != -1) {
          r = new Array();
        }
        r.push(node.text());
        if ($(n[i]).hasClass("route-active")) {
          break;
        }

      }
      return r;
    },
    pathClick : function (event) {
      $(".route-active").removeClass("route-active");
      $(this).addClass("route-active");
      var r = Dtc.path.pathToActive().join(" ");
      var active = $(r);
      var classSelectorDisabled = false;

      Dtc.editor.currentlyEditing['id'] = r;
      $("#class-selector").selectmenu("destroy");
      $("#class-selector").empty();
      $("#class-selector").append(
          '<option value="dtc-no-class">' + Drupal.t('Class...') + '</option>');

      if ($(this).text().charAt(0) == ".") {
        Dtc.editor.currentlyEditing['class'] = $(this).text();
        $.each(Dtc.editor.currentlyEditing['class'].split('.'), function (idx,
            elem) {
          if (elem.length > 0) {
            $("#class-selector").append(
                '<option value="' + elem + '">' + elem + '</option>');
          }
        });
      }
      else if (active.length == 1
          && Dtc.editor.getClass(active[0]) != "undefined") {
        Dtc.editor.currentlyEditing['class'] = Dtc.editor.getClass(active[0]);
        $.each(Dtc.editor.currentlyEditing['class'].split('.'), function (idx,
            elem) {
          if (elem.length > 0) {
            $("#class-selector").append(
                '<option value="' + elem + '">' + elem + '</option>');
          }
        });
      }
      else {
        Dtc.editor.currentlyEditing['class'] = "";
        classSelectorDisabled = true;
      }
      $("#class-selector").selectmenu({
        style : 'dropdown',
        width : 200
      });
      if (classSelectorDisabled) {
        $("#class-selector").selectmenu("disable");
      }

      // TODO need to check if it is a valid node.
      Dtc.editor.currentlyEditing['tag'] = active[0].tagName.toLowerCase();
      if (Dtc.editor.currentlyEditing['tag'] != 'a') {
        $("#status-link").button("disable");
        $("#status-active").button("disable");
        $("#status-visited").button("disable");
      }
      else {
        $("#status-link").button("enable");
        $("#status-active").button("enable");
        $("#status-visited").button("enable");
      }

      $(".selector").removeClass("dtc-ui-state-active");
      $("#class-selector-button").removeClass("dtc-ui-state-active");
      $("#tag-selector span").text(
          Drupal.t('All') + ' <' + Dtc.editor.currentlyEditing['tag'] + '>');
      $("#id-selector").addClass("dtc-ui-state-active");

      Dtc.fade.fadeRoute();
      Dtc.undoRedo.newGroup();
    },
    createPath : function (route) {
      var nodes = route.split(" ");
      $("#dtc-path button").button("destroy");
      $("#dtc-path").text("");
      $("#dtc-path").append("<button id='route-back'><</button>");
      $.each(nodes, function (idx, node) {
        $("<button class='route'>" + node + "</button>")
            .appendTo("#dtc-path");
      });
      $("<button class='route-new'>...</button>").appendTo("#dtc-path");
      $("#dtc-path button").button();
      $("#dtc-path button.route:last").addClass("route-active");
      $(".route").editable(
          {
            onSubmit : function (content) {
              if (!content.current.length) {
                if ($(".route").length == 1) {
                  Dtc.toolbar.showToolbarError(Drupal
                      .t("Can't remove all elements!"));
                  $(this).html(content.previous);
                  return;
                }
                else {
                  $(this).remove();
                  $(".route:last").addClass("route-active");
                }
              }
              else {
                $(this).html(
                    "<span class='ui-button-text'>" + content.current
                        + "</span>");
              }
              Dtc.fade.fadeRoute();
            },
            editBy : 'dblclick',
            onEdit : function (content) {
              var text = content.current.replace(/<[^<>]+>/g, "");
              $(this).find("input").val(text);
            }
          });
      $("#dtc-path button.route-new").editable(
          {
            onSubmit : function (content) {
              $(this)
                  .html(
                      "<span class='ui-button-text'>" + content.current
                          + "</span>");
              Dtc.path.addPathElement();
            },
            onEdit : function (content) {
              $(this).find("input").val("");
            }
          });
      $("#dtc-path button.route").click(Dtc.path.pathClick);
      $("#route-back")
          .click(
              function (e) {
                e.stopPropagation();
                var parent = $($("#dtc-path button.route:first span").text())[0].parentNode;
                var r = "";
                if (parent.tagName.toLowerCase() == "html") {
                  return;
                }

                while (parent.tagName.toLowerCase() != "html") {
                  r = Dtc.editor.getIdentifier(parent) + " " + r;
                  if (parent.id != "") {
                    break;
                  }

                  parent = parent.parentNode;
                }
                Dtc.path.createPath(r + route);
              });
    },
    addPathElement : function () {
      var o = $("#dtc-path button.route-new");
      var oSpan = $("#dtc-path button.route-new span");
      if (oSpan.text().length == 0) {
        oSpan.text("...");
        return;
      }
      // Add necessary classes.
      o.addClass("route").addClass("route-active").removeClass("route-new");
      // Remove events.
      o.editable("destroy");
      // Add events the the new element.
      o.editable({
        onSubmit : function (content) {
          $(this).html(
              "<span class='ui-button-text'>" + content.current + "</span>");
          Dtc.fade.fadeRoute();
        },
        editBy : 'dblclick',
        onEdit : function (content) {
          var text = content.current.replace(/<[^<>]+>/g, "");
          $(this).find("input").val(text);
        }
      });
      $("<button class='route-new'>...</button>").appendTo("#dtc-path");
      $("#dtc-path button.route-new").button();
      // Add events and new "..." element.
      $("#dtc-path button.route-new").editable(
          {
            onSubmit : function (content) {
              $(this)
                  .html(
                      "<span class='ui-button-text'>" + content.current
                          + "</span>");
              Dtc.path.addPathElement();
            },
            onEdit : function (content) {
              $(this).find("input").val("");
            }
          });
      o.click(Dtc.path.pathClick);

      // Fade the new route.
      o.click();
    }
  };
  Dtc.path = new Dtc.Path();

  Dtc.Fade = Class.create();
  Dtc.Fade.prototype = {
    initialize : function () {
      this.animating = false;
      this.savedBackground = "";
    },
    fadeRoute : function (content) {
      var route = new Array();
      var nodes = $("#dtc-path button.route");

      Dtc.toolbar.hideToolbarError();

      for ( var i = 0; i < nodes.length; i++) {
        route.push($(nodes[i]).find("span").text());
        if ($(nodes[i]).hasClass("route-active")) {
          break;
        }
      }
      Dtc.fade.doFade(route.join(" "));
    },
    /**
     * Fading the elements selected by the selector.
     *
     * @param selector
     *          The selector.
     */
    doFade : function (selector) {
      Dtc.loadCssProps();
      if (this.animating) {
        return false;
      }

      this.animating = true;
      var len = $(selector).length - 1;
      $(selector).each(function (idx, element) {
        if (element.id == '_firebugConsole') {
          return;
        }

        var before = $(this).css("background-color");
        if (before == "rgb(216, 241, 248)" || before == "#d8f1f8") {
          before = Dtc.fade.savedBackground;
        }
        $(this).animate({
          backgroundColor : "#479bff"
        }, 400).animate({
          backgroundColor : before
        }, 300, function () {
          $(this).css("background-color", "");
          if (idx == len) {
            Dtc.fade.animating = false;
          }

        });
      });
    }
  };
  Dtc.fade = new Dtc.Fade();

  Dtc.Reload = Class.create();
  Dtc.Reload.prototype = {
    initialize : function () {
      this.popups = 1;
    },
    reloadTheTheme : function (cookieContent) {
      if (!Dtc.editorOnly) {
        Dtc.config.data = $.evalJSON(this.cookieContent);
      }
      else {
        Dtc.config.restore();
      }
      $.each(Dtc.config.data, function (i, val) {
        if (i == "css") {
          $.each(val, function (what, array) {
            $.each(array, function (property, value) {
              Dtc.css(what, property, value);
            });
          });
        }
      });
    },
    enableSave : function () {
      var self = this;
      if (!Dtc.editorOnly) {
        window.onbeforeunload = function () {
          if (Dtc.config.modified) {
            Dtc.config.store();
            if (self.popups) {
              return Drupal.t("Your modifications will be saved. You can load them back later if you want.");
            }
          }
        };
      }
      else {
        window.onbeforeunload = function () {
          if (Dtc.config.modified) {
            Dtc.config.store();
          }
        };
      }
    },
    enable : function () {
      var self = this;

      this.enableSave();
      if (Drupal.settings.dtc != null && Drupal.settings.dtc.developer != 0) {
        if (!($.cookie('dtc_popups'))) {
          this.popups = 0;
        }
      }
      // Checkbox handler to set the value of popups.
      $('#popups-onoff').click(function () {
        self.popups = (this.checked == 1) ? 1 : 0;
        $.cookie('dtc_popups', self.popups);
      });
    }
  };
  Dtc.reload = new Dtc.Reload();

  Dtc.StyleCommand = Class.create();
  Dtc.StyleCommand.prototype = {
    initialize : function (selector, prop, val) {
      this.mSelector = selector;
      this.mProp = prop;
      this.mVal = val;
      this.mOldVal = $(selector).css(prop);
    },
    setSelector : function (selector) {
      this.mSelector = selector;
    },
    execute : function () {
      Dtc.css(this.mSelector, this.mProp, this.mVal);
      Dtc.config.pushCss(this.mSelector, this.mProp, this.mVal);
    },
    undo : function () {
      Dtc.css(this.mSelector, this.mProp, this.mOldVal);
      Dtc.config.pushCss(this.mSelector, this.mProp, this.mOldVal);
      Dtc.toolbar.get(this.mProp).setValue(this.mOldVal);
    },
    redo : function () {
      this.execute();
      Dtc.toolbar.get(this.mProp).setValue(this.mVal);
    }
  };

  Dtc.CommandGroup = Class.create();
  Dtc.CommandGroup.prototype = {
    initialize : function (refresh) {
      this.refresh = refresh;
      this.commands = [];
      this.actual = 0;
      this.currentlyEditing = {};
      this.currentlyEditing['id'] = Dtc.editor.currentlyEditing['id'];
      this.currentlyEditing['tag'] = Dtc.editor.currentlyEditing['tag'];
      this.currentlyEditing['class'] = Dtc.editor.currentlyEditing['class'];
      this.currentlySelected = Dtc.editor.currentlySelected;
    },
    add : function (c) {
      this.commands[this.actual] = c;
      this.actual++;
      if (this.commands.length > this.actual) {
        this.commands.splice(this.actual);
      }
    },
    canUndo : function () {
      if (this.commands.length == 0 || this.actual == 0) {
        return false;
      }

      return true;
    },
    undo : function () {
      if (!this.canUndo()) {
        return false;
      }

      this.actual--;
      this.commands[this.actual].undo();
      return true;
    },
    canRedo : function () {
      if (this.commands.length == 0 || this.actual == this.commands.length) {
        return false;
      }

      return true;
    },
    redo : function () {
      if (!this.canRedo()) {
        return false;
      }

      this.commands[this.actual].redo();
      this.actual++;
      return true;
    },
    refreshEditor : function () {
      if (this.refresh) {
        Dtc.editor.setCurrently(this.currentlyEditing, this.currentlySelected);
        Dtc.loadCssProps();
      }
    },
    setSelector : function () {
      var selector = this.currentlyEditing[this.currentlySelected];
      for ( var i in this.commands) {
        this.commands[i].setSelector(selector);
      }
    },
    getCommands : function () {
      var c = [];
      for ( var i = 0; i < this.actual; i++) {
        c[i] = jQuery.extend(true, {}, this.commands[i]);
      }
      return c;
    },
    setCommands : function (commands) {
      this.commands = commands;
      this.actual = commands.length;
    },
    executeAll : function () {
      for ( var i = 0; i < this.actual; i++) {
        this.commands[i].execute();
      }
    },
    undoAll : function () {
      for ( var i = this.actual - 1; i >= 0; i--) {
        this.commands[i].undo();
      }
    }
  };

  Dtc.UndoRedo = Class.create();
  Dtc.UndoRedo.prototype = {
    initialize : function () {
      this.groups = [];
      // Points to the next insertable place.
      this.actual = 0; 
      this.modified = false;
      // Add initial group.
      this.addGroup(new Dtc.CommandGroup()); 
    },
    addGroup : function (g) {
      this.groups[this.actual] = g;
      this.actual++;
      if (this.groups.length > this.actual) {
        this.groups.splice(this.actual);
      }
    },
    newGroup : function () {
      var refresh = false;
      if (!arguments.length) {
        refresh = true;
      }

      if (this.modified) {
        this.addGroup(new Dtc.CommandGroup(refresh));
        this.modified = false;
      }
      else {
        this.groups[this.actual - 1].initialize(refresh);
      }
    },
    addCommand : function (c) {
      /*
       * Create a new group for the modifications if the apply button is
       * enabled. (This means that the user changed the selector)
       */
      if (!$("#apply-button").button("option", "disabled")) {
        this.newGroup();
        $("#apply-button").button("disable");
      }
      this.groups[this.actual - 1].add(c);
      this.modified = true;

      c.execute();
    },
    undo : function () {
      if (this.actual == 1 && !this.groups[this.actual - 1].canUndo()) {
        return;
      }

      var r = this.groups[this.actual - 1].undo();
      if (!r) {
        this.actual--;
        if (this.actual == 0) {
          this.actual = 1;
          return;
        }
        this.groups[this.actual - 1].refreshEditor();
        this.undo();
      }
      // We move to the previous group if the group hasn't got any undoable
      // element.
      if (!this.groups[this.actual - 1].canUndo()) {
        this.actual--;
        if (this.actual == 0) {
          this.actual = 1;
        }
        this.groups[this.actual - 1].refreshEditor();
      }
    },
    redo : function () {
      if (this.actual == this.groups.length && !this.groups[this.actual - 1].canRedo()) {
        return;
      }

      var r = this.groups[this.actual - 1].redo();
      if (!r) {
        this.actual++;
        if (this.actual > this.groups.length) {
          this.actual = this.groups.length;
          return;
        }
        this.groups[this.actual - 1].refreshEditor();
        this.redo();
      }
    },
    enable : function () {
      $("#undo").click(function () {
        Dtc.undoRedo.undo();
      });
      $("#redo").click(function () {
        Dtc.undoRedo.redo();
      });
      var self = this;
      $("#apply-button").click(function () {
        var g = self.groups[self.actual - 1];
        self.newGroup();
        self.groups[self.actual - 1].setCommands(g.getCommands());
        self.groups[self.actual - 1].setSelector();
        self.groups[self.actual - 1].executeAll();
        $(this).button("disable");
      });
    }
  };
  Dtc.undoRedo = new Dtc.UndoRedo();
})(jQuery);
