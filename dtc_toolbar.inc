<?php
// $Id$
/**
 * @file
 * The toolbar's elements.
 *
 * Copyright 2010 by Department of Software Engineering, University of Szeged, Hungary
 */

function dtc_toolbar_form($form_state) {
  $form = array();
  // The interface.
  dtc_toolbar_interface($form);

  // The basic tab.
  dtc_toolbar_basic_tab($form);

  // The background tab.
  dtc_toolbar_background_tab($form);

  // The border tab.
  dtc_toolbar_border_tab($form);

  // The layout tab.
  dtc_toolbar_layout_tab($form);

  // The advanced tab.
  dtc_toolbar_advanced_tab($form);

  return $form;
}

function dtc_toolbar_interface(&$form) {
  $form['path'] = array(
    '#type' => 'markup',
    '#value' =>
         '<div id="dtc-scroll-left"></div>
          <div id="dtc-scroll-right"></div>
          <div id="dtc-path"></div>',
    '#prefix' => '<div id="dtc-path-wrapper">',
    '#suffix' => '</div>'
  );
  $form['status-modifier']['label'] = array(
    '#type' => 'markup',
    '#value' => '<div class="dtc-floating-label">' . t('Status') . ':</div>',
  );
  $form['status-modifier']['field'] = array(
    '#type' => 'select',
    '#options' => array(
      'none' => 'none',
      'hover' => ':hover',
      'link' => ':link',
      'visited' => ':visited',
      'active' => ':active',
    ),
    '#prefix' => '<div id="status-modifiers">',
    '#suffix' => '</div>',
    '#name' => 'status',
    '#id' => 'status-select',
    '#attributes' => array('class' => 'toolbar-element'),
  );
  $form['selector']['id'] = array(
    '#type' => 'button',
    '#value' => t('Just this'),
    '#id' => 'id-selector',
    '#name' => 'id-selector',
    '#attributes' => array('class' => 'selector selector-button'),
  );
  $form['selector']['tag'] = array(
    '#type' => 'button',
    '#value' => t('All'),
    '#id' => 'tag-selector',
    '#name' => 'tag-selector',
    '#attributes' => array('class' => 'selector selector-button'),
  );
  $form['selector']['class'] = array(
    '#type' => 'select',
    '#options' => array(
      'dtc-no-class' => t('Class...'),
    ),
    '#id' => 'class-selector',
    '#name' => 'class-selector',
  );

  $form['apply'] = array(
    '#type' => 'button',
    '#value' => t('Apply changes'),
    '#id' => 'apply-button',
    '#name' => 'apply-button',
  );
}

function dtc_toolbar_basic_tab(&$form) {
  $p = drupal_get_path('module', 'dtc') . '/images';

  $form['text-decoration'] = array(
    '#title' => t('Text decoration'),
    '#type' => 'checkboxes',
    '#options' => array(
      'font-weight-bold' => 'abc',
      'font-style-italic' => 'abc',
      'text-decoration-underline' => 'abc',
      'text-decoration-overline' => 'abc',
      'text-decoration-line-through' => 'abc',
      'text-decoration-blink' => 'abc',
    ),
    '#prefix' => '<div id="text-decoration" class="dtc-floating-form-item">',
    '#suffix' => '</div>',
  );

  $form['text-align'] = array(
    '#type' => 'radios',
    '#title' => t('Text align'),
    '#options' => array(
      'left' => theme_image("$p/icons/alignleft_26x26.png"),
      'center' => theme_image("$p/icons/alignhorizontalcenter_26x26.png"),
      'right' => theme_image("$p/icons/alignright_26x26.png"),
      'justify' => theme_image("$p/icons/alignblock_26x26.png"),
    ),
    '#prefix' => '<div id="text-align" class="dtc-floating-form-item">',
    '#suffix' => theme('dtc_popup_help', 'basic-text-align'),
  );

  $form['vertical-align'] = array(
    '#type' => 'radios',
    '#title' => t('Text valign'),
    '#options' => array(
      'top' => theme_image("$p/icons/aligntop_26x26.png"),
      'middle' => theme_image("$p/icons/alignvcenter_26x26.png"),
      'bottom' => theme_image("$p/icons/alignbottom_26x26.png"),
    ),
    '#prefix' => '<div id="text-valign" class="dtc-floating-form-item">',
    '#suffix' => theme('dtc_popup_help', 'basic-vertical-align'),
  );

  $form['font-size']['label'] = array(
    '#type' => 'markup',
    '#value' => theme_image("$p/icons/fontheight_26x26.png"),
  );
  $form['font-size']['field'] = array(
    '#type' => 'markup',
    '#value' => '<span id="font-size-field" >10</span>',
  );
  $form['font-size']['widget'] = array(
    '#type' => 'markup',
    '#value' => '<div id="font-size-slider"></div>',
  );

  $form['font-family']['label'] = array(
    '#type' => 'markup',
    '#value' => theme_image("$p/icons/fontname_26x26.png"),
  );
  $form['font-family']['field'] = array(
    '#type' => 'select',
    '#options' => dtc_get_fontfamilys(),
    '#attributes' => array(
      'class' => 'toolbar-element',
    ),
    '#id' => 'font-family-field',
  );
  $form['color']['label'] = array(
    '#type' => 'markup',
    '#value' => theme_image("$p/icons/fontcolor_26x26.png"),
  );
  $form['color']['field'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#attributes' => array(
      'class' => "toolbar-element color {hash:true,caps:false,pickerFaceColor:'#000000',pickerMode:'HVS'}",
    ),
    '#name' => 'color',
  );
}

function dtc_toolbar_background_tab(&$form) {
  $form['background-color'] = array(
    '#title' => t('Background color') . theme('dtc_popup_help', 'background-color'),
    '#type' => 'textfield',
    '#size' => 10,
    '#attributes' => array(
      'class' => "toolbar-element color {hash:true,caps:false,pickerFaceColor:'#000000',pickerMode:'HVS'}",
    ),
    '#name' => 'background-color',

  );
  $form['background-image'] = array(
    '#type' => 'textfield',
    '#title' => t('Image url') . theme('dtc_popup_help', 'background-image'),
    '#id' => 'background-image-field',
    '#name' => 'background-image',
    '#size' => 20,
  );
  $form['background-attachment'] = array(
    '#type' => 'radios',
    '#title' => t('Background attachment') . theme('dtc_popup_help', 'background-attachment'),
    '#options' => array(
      'scroll' => 'scroll',
      'fixed' => 'fixed',
    ),
    '#attributes' => array(
      'name' => 'background-attachment',
    ),
    '#prefix' => '<div id="background-attachment" class="dtc-floating-form-item">',
    '#suffix' => '</div',
  );

  $form['background-repeat'] = array(
    '#type' => 'radios',
    '#title' => t('Background repeat') . theme('dtc_popup_help', 'background-repeat'),
    '#options' => array(
      'repeat' => 'repeat',
      'repeat-x' => 'repeat-x',
      'repeat-y' => 'repeat-y',
      'no-repeat' => 'no-repeat',
    ),
    '#prefix' => '<div id="background-repeat" class="dtc-floating-form-item">',
    '#suffix' => '</div',
  );

  $metric_units = array(
    'px' => 'px',
    '%' => '%',
    'em' => 'em',
  );
  $labels = array(
    'x' => t('Background position (x)'),
    'y' => t('Background position (y)'),
  );
  foreach ($labels as $d => $label) {
    $form["background-position-$d"]['label'] = array(
      '#type' => 'markup',
      '#value' => '<span id="background-position-' . $d . '-label">' . $label . theme('dtc_popup_help', "background-position-$d") . ':</span>',
    );
    $form["background-position-$d"]['field'] = array(
      '#type' => 'textfield',
      '#attributes' => array(
        'class' => 'toolbar-element background-position',
      ),
      '#id' => "background-position-$d-value",
      '#name' => "background-position-$d-value",
      '#size' => 3,
      '#value' => 0,
    );
    $form["background-position-$d"]['unit'] = array(
      '#type' => 'select',
      '#attributes' => array(
        'class' => 'toolbar-element background-position',
      ),
      '#name' => "background-position-$d-unit",
      '#id' => "background-position-$d-unit",
      '#options' => $metric_units,
    );
  }
}

function dtc_toolbar_border_tab(&$form) {
  $p = drupal_get_path('module', 'dtc') . '/images';

  $form['border']['label'] = array(
    '#type' => 'markup',
    '#value' => t('Border style') . ':',
    '#prefix' => '<div class="dtc-label">',
    '#suffix' => '</div>',
  );
  $form['border']['lock'] = array(
    '#type' => 'checkbox',
    '#title' => t('Lock'),
    '#name' => 'border-style-lock',
    '#id' => 'border-style-lock',
    '#attributes' => array('class' => 'toolbar-element lock-checkbox'),
  );
  $form['border-width']['label'] = array(
    '#type' => 'markup',
    '#value' => theme_image("$p/icons/setborderstyle_26x26.png"),
  );
  foreach (array('left', 'right', 'top', 'bottom') as $d) {
    $form['border-width'][$d]['field'] = array(
      '#type' => 'markup',
      '#value' => '<div id ="border-' . $d . '-width-field" class="dtc-float-left" >0</div>',
    );
    $form['border-width'][$d]['widget'] = array(
      '#type' => 'markup',
      '#value' => '<div id="border-' . $d . '-width-slider" class="slider-type-3"></div>',
    );
  }

  $border_style_options = array(
    'solid'  => 'solid' ,
    'dashed' => 'dashed',
    'dotted' => 'dotted',
    'double' => 'double',
    'groove' => 'groove',
    'ridge'  => 'ridge',
    'inset'  => 'inset',
    'outset' => 'outset'
  );

  $form['border-style']['label'] = array(
    '#type' => 'markup',
    '#value' => theme_image("$p/icons/linestyle_26x26.png"),
  );
  foreach (array('left', 'right', 'top', 'bottom') as $d) {
    $form['border-style'][$d] = array(
      '#type' => 'select',
      '#options' => $border_style_options,
      '#name' => 'border-' . $d . '-style',
      '#attributes' => array('class' => 'toolbar-element'),
      '#id' => 'border-' . $d . '-style-field'
    );
  }
  $form['border-color']['label'] = array(
    '#type' => 'markup',
    '#value' => theme_image("$p/icons/colorsettings_26x26.png"),
  );
  foreach (array('left', 'right', 'top', 'bottom') as $d) {
    $form['border-color'][$d] = array(
      '#type' => 'textfield',
      '#size' => 10,
      '#attributes' => array(
        'class' => "toolbar-element color {hash:true,caps:false,pickerFaceColor:'#000000',pickerMode:'HVS'}"
      ),
      '#name' => 'border-' . $d . '-color',
    );
  }

  $form['border-radius']['label'] = array(
    '#type' => 'markup',
    '#value' => t('Border radius') . ':' . theme('dtc_popup_help', 'border-radius'),
    '#prefix' => '<div class="dtc-label">',
    '#suffix' => '</div>',
  );
  $form['border-radius']['lock'] = array(
    '#type' => 'checkbox',
    '#title' => t('Lock'),
    '#name' => 'border-radius-lock',
    '#id' => 'border-radius-lock',
    '#attributes' => array('class' => 'toolbar-element lock-checkbox'),
  );
  foreach (array('top-left', 'top-right', 'bottom-left', 'bottom-right') as $d) {
    $form['border-radius'][$d]['field'] = array(
      '#type' => 'textfield',
      '#name' => "border-$d-radius",
      '#id' => "border-$d-radius-field",
      '#size' => 3,
      '#value' => 0,
      '#attributes' => array(
        'class' => 'dtc-float-left dtc-width-10',
      ),
    );
    $form['border-radius'][$d]['widget'] = array(
      '#type' => 'markup',
      '#value' => '<div name="border-' . $d . '-radius" class="slider-type-3" id="border-' . $d . '-radius-slider"></div>',
    );
  }
}

function dtc_toolbar_layout_tab(&$form) {
  $labels = array(
    'top' => t('Top') . ':',
    'left' => t('Left') . ':',
    'bottom' => t('Bottom') . ':',
    'right' => t('Right') . ':',
  );
  $properties = array(
    'padding' => t('Padding'),
    'margin' => t('Margin'),
  );
  foreach ($properties as $prop => $prop_name) {
    $form[$prop]['label'] = array(
      '#type' => 'markup',
      '#value' => $prop_name . theme('dtc_popup_help', "layout-$prop")
    );
    $form[$prop]['lock'] = array(
      '#type' => 'checkbox',
      '#title' => t('Lock'),
      '#name' => "$prop-lock",
      '#id' => "$prop-lock",
      '#attributes' => array('class' => 'toolbar-element lock-checkbox'),
    );
    foreach ($labels as $d => $label) {
      $form['padding'][$d]['label'] = array(
        '#type' => 'markup',
        '#value' => $label,
        '#prefix' => '<div class="dtc-label">',
        '#suffix' => '</div>',
      );

      $form[$prop][$d]['field'] = array(
        '#type' => 'textfield',
        '#size' => 3,
        '#name' => "$prop-$d-field",
        '#id' => "$prop-$d-field",
        '#default_value' => 0,
      );
      $form[$prop][$d]['widget'] = array(
        '#type' => 'markup',
        '#value' => '<div name="' . $prop . '-' . $d . '" class="slider-type-4" id="' . $prop . '-' . $d . '-slider"></div>',
      );
    }
  }
  $clear_options = array(
    'none' => 'none',
    'left' => 'left',
    'right' => 'right',
    'both' => 'both',
  );
  $form['clear'] = array(
    '#type' => 'select',
    '#title' => t('Clear'),
    '#name' => 'clear',
    '#id' => 'clear-field',
    '#attributes' => array('class' => 'toolbar-element'),
    '#options' => $clear_options,
    '#prefix' => '<div class="dtc-bordered">',
    '#suffix' => '</div>',
  );
  $float_options = array(
    'none' => 'none',
    'left' => 'left',
    'right' => 'right',
  );
  $form['float'] = array(
    '#type' => 'select',
    '#title' => t('Float'),
    '#name' => 'float',
    '#id' => 'float-field',
    '#options' => $float_options,
    '#attributes' => array('class' => 'toolbar-element'),
    '#prefix' => '<div class="dtc-bordered">',
    '#suffix' => '</div>',
  );

  $form['display'] = array(
    '#prefix' => '<div class="dtc-bordered">',
    '#suffix' => '</div>',
  );
  $form['display']['field'] = array(
    '#type' => 'select',
    '#title' => t('Display'),
    '#name' => 'display',
    '#id' => 'display-field',
    '#attributes' => array('class' => 'toolbar-element'),
    '#options' => array(
      'none' => 'none',
      'block' => 'block',
      'inline' => 'inline',
      'inline-block' => 'inline-block',
      'inline-table' => 'inline-table',
      'list-item' => 'list-item',
      'run-in' => 'run-in',
      'table' => 'table',
      'table-cell' => 'table-cell',
      'table-caption' => 'table-caption',
      'table-column' => 'table-column',
      'table-column-group' => 'table-column-group',
      'table-footer-group' => 'table-footer-group',
      'table-header-group' => 'table-header-group',
      'table-row' => 'table-row',
      'table-row-group' => 'table-row-group',
    ),
  );
  $form['display']['restore'] = array(
    '#type' => 'button',
    '#value' => t('Restore all hidden elements'),
    '#id' => 'restore-hidden',
    '#attributes' => array('class' => 'toolbar-element'),
  );

  $labels = array(
    'width' => t('Width') . ':',
    'height' => t('Height') . ':',
  );
  foreach ($labels as $d => $label) {
    $form[$d]['label'] = array(
      '#type'  => 'markup',
      '#value' => $label,
      '#prefix' => '<div class="dtc-label">',
      '#suffix' => '</div>',
    );
    $form[$d]['widget'] = array(
      '#type' => 'markup',
      '#value' => '<div name="' . $d . '" class="slider-type-2" id="' . $d . '-slider"></div>'
    );
    $form[$d]['field'] = array(
      '#type' => 'textfield',
      '#size' => 3,
      '#name' => "$d-field",
      '#id' => "$d-field",
      '#value' => 0,
      '#attributes' => array('class' => 'toolbar-element')
    );
    $form[$d]['state'] = array(
      '#type'  => 'markup',
      '#value' => l(t('Dynamic'), '', array('attributes' => array('id' => "$d-state-flag"))),
    );
  }
}

function dtc_toolbar_advanced_tab(&$form) {
  $text_transform_options = array(
    'none' => 'none',
    'uppercase' => 'ABC',
    'lowercase' => 'abc',
    'capitalize' => 'Abc',
  );
  $form['text-transform'] = array(
    '#type' => 'radios',
    '#title' => t('Text transform'),
    '#prefix' => '<div id="text-transform" class="dtc-floating-form-item">',
    '#suffix' => theme('dtc_popup_help', 'text-transform') . '</div>',
    '#options' => $text_transform_options,
    '#name' => 'text-transform',
    '#title' => t('Text transform'),
    '#default value' => 'none',
  );

  $text_direction_options = array(
    'rtl' => 'RTL',
    'ltr' => 'LTR',
  );
  $form['direction'] = array(
    '#type' => 'radios',
    '#title' => t('Text direction'),
    '#prefix' => '<div id="text-direction" class="dtc-floating-form-item">',
    '#suffix' => theme('dtc_popup_help', 'text-direction') . '</div>',
    '#options' => $text_direction_options,
    '#title' => t('Text direction'),
    '#default value' => 'ltr',
  );

  $labels = array(
    'line-height' => t('Line height') . ':' . theme('dtc_popup_help', 'advanced-line-height'),
    'word-spacing' => t('Word spacing') . ':' . theme('dtc_popup_help', 'advanced-word-spacing'),
    'letter-spacing' => t('Letter spacing') . ':' . theme('dtc_popup_help', 'advanced-letter-spacing'),
    'text-indent' => t('Text indent') . ':' . theme('dtc_popup_help', 'advanced-indentation'),
  );
  foreach ($labels as $d => $label) {
    $form[$d]['label'] = array(
      '#type' => 'markup',
      '#value' => $label,
      '#prefix' => '<div class="dtc-label">',
      '#suffix' => '</div>'
    );
    $form[$d]['field'] = array(
      '#type' => 'markup',
      '#value' => '<span name="' . $d . '" id="' . $d . '-field">0</span> px',
    );
    $form[$d]['widget'] = array(
      '#type' => 'markup',
      '#value' => '<div class="slider-type-1" id="' . $d . '-slider"></div>',
    );
  }
}

function theme_dtc_toolbar_form($form) {
  $output = '<div id="dtc-toolbar">';
  $output .= '<div id="selector-wrapper">';
  $output .= '<div class="smoothness">';
  $output .= drupal_render($form['path']) . drupal_render($form['status-modifier']['label']) . drupal_render($form['status-modifier']['field']);
  // End of smoothness.
  $output .= '</div>';

  $rows = array();
  $rows[] = array(
    drupal_render($form['selector']['id']),
    drupal_render($form['selector']['tag']),
    drupal_render($form['selector']['class']),
    drupal_render($form['apply']),
  );

  $output .= '<div id="selector-buttons">' . theme_table(array(), $rows) . '</div>';
  // End of selector wrapper.
  $output .= '</div>';
  $output .= '<div id="toolbar-content">';
  // The headers.
  $output .=
   '<ul id="tabs-header">' .
     '<li><a href="#toolbar-basic">' . t('Basic') . '</a></li>
      <li><a href="#toolbar-background-settings">' . t('Background') . '</a></li>
      <li><a href="#toolbar-border-settings">' . t('Border') . '</a></li>
      <li><a href="#toolbar-layout">' . t('Layout') . '</a></li>
      <li><a href="#toolbar-advanced">' . t('Advanced') . '</a></li>
    </ul>';


  // The basic tab.
  $output .= '<div id="toolbar-basic">';
  $rows = array();
  $rows[] = array(
    array('data' => drupal_render($form['text-decoration']), 'colspan' => 7),
  );
  $rows[] = array(
    array('data' => drupal_render($form['text-align']), 'colspan' => 3),
    array('data' => drupal_render($form['vertical-align']), 'colspan' => 4),
  );
  $rows[] = array(
    array('data' => drupal_render($form['font-size']['label']), 'style' => 'width: 5%;'),
    array('data' => drupal_render($form['font-size']['field']), 'style' => 'width: 5%;'),
    array('data' => drupal_render($form['font-size']['widget']), 'style' => 'width: 30%;'),
    drupal_render($form['font-family']['label']),
    drupal_render($form['font-family']['field']),
    drupal_render($form['color']['label']),
    drupal_render($form['color']['field']),
  );
  $output .= theme_table(array(), $rows);
  $output .= '</div>';
  // The background tab.
  $output .= '<div id="toolbar-background-settings">';
  $rows = array();
  $rows[] = array(
    array('data' => drupal_render($form['background-color']), 'colspan' => 2),
    array('data' => drupal_render($form['background-image']), 'colspan' => 2),
  );
  $rows[] = array(
    array('data' => drupal_render($form['background-attachment']), 'colspan' => 2),
    array('data' => drupal_render($form['background-repeat']), 'colspan' => 2),
  );
  $rows[] = array(
    array('data' => drupal_render($form['background-position-x']['label']), 'colspan' => 2),
    array('data' => drupal_render($form['background-position-y']['label']), 'colspan' => 2),
  );
  $rows[] = array(
    array(
      'data' => drupal_render($form['background-position-x']['field']),
      'class' => 'dtc-width-10',
    ),
    array(
      'data' => drupal_render($form['background-position-x']['unit']),
      'class' => 'dtc-width-25'
    ),
    array(
      'data' => drupal_render($form['background-position-y']['field']),
      'class' => 'dtc-width-10'
    ),
    array(
      'data' => drupal_render($form['background-position-y']['unit']),
      'class' => 'dtc-width-55'
    ),
  );
  $output .= theme_table(array(), $rows);
  $output .= '</div>';
  // The border tab.
  $output .= '<div id="toolbar-border-settings">';
  $rows = array();
  $rows[] = array(
    drupal_render($form['border']['lock']),
    array('data' => t('Top'), 'class' => 'dtc-table-header'),
    array('data' => t('Left'), 'class' => 'dtc-table-header'),
    array('data' => t('Bottom'), 'class' => 'dtc-table-header'),
    array('data' => t('Right'), 'class' => 'dtc-table-header'),
  );
  $rows[] = array(
    drupal_render($form['border-width']['label']),
    drupal_render($form['border-width']['top']['field']) .
    drupal_render($form['border-width']['top']['widget']),
    drupal_render($form['border-width']['left']['field']) .
    drupal_render($form['border-width']['left']['widget']),
    drupal_render($form['border-width']['bottom']['field']) .
    drupal_render($form['border-width']['bottom']['widget']),
    drupal_render($form['border-width']['right']['field']) .
    drupal_render($form['border-width']['right']['widget']),
  );
  $rows[] = array(
    drupal_render($form['border-style']['label']),
    drupal_render($form['border-style']['top']),
    drupal_render($form['border-style']['left']),
    drupal_render($form['border-style']['bottom']),
    drupal_render($form['border-style']['right']),
  );
  $rows[] = array(
    drupal_render($form['border-color']['label']),
    drupal_render($form['border-color']['top']),
    drupal_render($form['border-color']['left']),
    drupal_render($form['border-color']['bottom']),
    drupal_render($form['border-color']['right']),
  );
  $border = drupal_render($form['border']['label']) .
            theme_table(array(), $rows, array('class' => 'style-1'));

  $rows = array();
  $rows[] = array(
    drupal_render($form['border-radius']['lock']),
    array('data' => t('Top-left'), 'class' => 'dtc-table-header'),
    array('data' => t('Top-right'), 'class' => 'dtc-table-header'),
    array('data' => t('Bottom-left'), 'class' => 'dtc-table-header'),
    array('data' => t('Bottom-right'), 'class' => 'dtc-table-header'),
  );
  $rows[] = array(
    '',
    drupal_render($form['border-radius']['top-left']['field']) .
    drupal_render($form['border-radius']['top-left']['widget']),
    drupal_render($form['border-radius']['top-right']['field']) .
    drupal_render($form['border-radius']['top-right']['widget']),
    drupal_render($form['border-radius']['bottom-left']['field']) .
    drupal_render($form['border-radius']['bottom-left']['widget']),
    drupal_render($form['border-radius']['bottom-right']['field']) .
    drupal_render($form['border-radius']['bottom-right']['widget']),
  );
  $border_radius = drupal_render($form['border-radius']['label']) .
    theme_table(array(), $rows, array('class' => 'style-1'));

  $rows = array();
  $rows[] = array(
    $border,
  );
  $rows[] = array(
    $border_radius,
  );
  $output .= theme_table(array(), $rows);
  // End of border tab.
  $output .= '</div>';
  // The layout tab
  $output .= '<div id="toolbar-layout">';
  // Padding and margin.
  $rows = array();
  $rows[] = array(
    '',
    drupal_render($form['padding']['lock']),
    array('data' => drupal_render($form['padding']['label']), 'class' => 'dtc-table-header'),
    drupal_render($form['margin']['lock']),
    array('data' => drupal_render($form['margin']['label']), 'class' => 'dtc-table-header'),
  );
  foreach (array('left', 'right', 'top', 'bottom') as $d) {
    $rows[] = array(
      drupal_render($form['padding'][$d]['label']),
      drupal_render($form['padding'][$d]['field']),
      drupal_render($form['padding'][$d]['widget']),
      drupal_render($form['margin'][$d]['field']),
      drupal_render($form['margin'][$d]['widget']),
    );
  }

  $padding = theme_table($headers, $rows, array('class' => 'style-1'));
  // Dimension
  $rows = array();
  foreach (array('width', 'height') as $d) {
    $rows[] = array(
      drupal_render($form[$d]['label']),
      drupal_render($form[$d]['field']),
      drupal_render($form[$d]['widget']),
      drupal_render($form[$d]['state']),
    );
  }

  $dimension = theme_table(array(), $rows, array('class' => 'style-1'));

  $rows = array();
  $rows[] = array(
    array('data' => $padding, 'rowspan' => 2),
    array('data' => drupal_render($form['clear']), 'class' => 'style-1'),
    array('data' => drupal_render($form['display']), 'rowspan' => 2, 'class' => 'style-1'),
  );
  $rows[] = array(
    array('data' => drupal_render($form['float']), 'class' => 'style-1'),
  );
  $rows[] = array(
    array('data' => $dimension, 'colspan' => 3),
  );

  $output .= theme_table(array(), $rows);
  //End of layout tab.
  $output .= '</div>';

  // The advanced tab.
  $output .= '<div id="toolbar-advanced">';
  $rows = array();
  $rows[] = array(
    array('data' => drupal_render($form['text-transform']), 'colspan' => 2),
    array('data' => drupal_render($form['direction']), 'colspan' => 2),
  );
  $rows[] = array(
    array('data' => drupal_render($form['text-indent']['label']), 'colspan' => 2),
    array('data' => drupal_render($form['line-height']['label']), 'colspan' => 2),
  );
  $rows[] = array(
    drupal_render($form['text-indent']['field']),
    drupal_render($form['text-indent']['widget']),
    drupal_render($form['line-height']['field']),
    drupal_render($form['line-height']['widget']),
  );
  $rows[] = array(
    array('data' => drupal_render($form['word-spacing']['label']), 'colspan' => 2),
    array('data' => drupal_render($form['letter-spacing']['label']), 'colspan' => 2),
  );
  $rows[] = array(
    drupal_render($form['word-spacing']['field']),
    drupal_render($form['word-spacing']['widget']),
    drupal_render($form['letter-spacing']['field']),
    drupal_render($form['letter-spacing']['widget']),
  );

  $output .= theme_table(array(), $rows);
  // End of advanced tab.
  $output .= '</div>';
  // End of tab wrapper.
  $output .= '</div>';
  // End of toolbar wrapper.
  $output .= '</div>';
  $output .= drupal_render($form);
  return $output;
}

function dtc_get_fontfamilys() {
  $ff['Georgia, serif'] = 'Georgia';
  $ff['"Times New Roman", Times, serif'] = 'Times New Roman';
  $ff['"Palatino Linotype", "Book Antiqua", Palatino, serif'] = 'Palatino Linotype';
  $ff['Arial, Helvetica, sans-serif'] = 'Arial';
  $ff['Arial Black, Gadget, sans-serif'] = 'Arial Black';
  $ff['"Comic Sans MS", cursive, sans-serif'] = 'Comic Sans MS';
  $ff['Impact, Charcoal, sans-serif'] = 'Impact';
  $ff['"Lucida Sans Unicode", "Lucida Grande", sans-serif'] = 'Lucida Sans Unicode';
  $ff['Tahoma, Geneva, sans-serif'] = 'Tahoma';
  $ff['"Trebuchet MS", Helvetica, sans-serif'] = 'Trebuchet MS';
  $ff['Verdana, Geneva, sans-serif'] = 'Verdana';
  $ff['"Courier New", Courier, monospace'] = 'Courier New';
  $ff['"Lucida Console", Monaco, monospace'] = 'Lucida Console';
  return $ff;

}
