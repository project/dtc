<?php
// $Id$

/**
 * @file
 * Contains the administration pages of the module.
 * The administration section can be found at admin/settings/dtc.
 */

/**
 * Form of the settings page at admin/settings/dtc.
 * Handles the turn on/off options.
 */
function dtc_settings_form() {
  $form = array();
  $form['#submit'][] = 'dtc_settings_form_submit';
  $form['dtc_status'] = array(
    '#title' => t('Status'),
    '#type' => 'radios',
    '#options' => array(
      'on' => t('On'),
      'off' => t('Off'),
    ),
    '#default_value' => variable_get('dtc_status', 'off'),
  );
  return system_settings_form($form);
}

/**
 *	Additional form submit for dtc_settings_form.
 */
function dtc_settings_form_submit($form, &$form_state) {
  dtc_set_status($form_state['values']['dtc_status']);
}