// $Id$

README.txt
======================

DTC module allows you to modify your theme by a WYSIWYG editor. Your 
modifications will be saved in css files, which you can turn on/off on the 
admin pages.

IMPORTANT! Read the notes at the INSTALLATION section of this file!

Visit our site at http://drupalthemecreator.org if you want to create a full 
theme from the scratch.

You can enable the editor in the System configuration page under Dtc settings.
Grant the access permission of the module to be able to access the edit mode.


INSTALLATION:
  The module depends on:
   * jQuery Update 6.x-2.x
   * jQuery UI with 1.7.x UI javascript library
  You have to install them first.
  The module needs some more 3rd party javascript libraries. We have made a zip 
  file  which contains all of the libraries you will need. 
  We recommend to download this and unzip its content to the module's directory.
  You can find this file at:
    http://drupalthemecreator.org/dtc-module
  The zip file contains a plugins directory which you have to unzip to the 
  module's root. You have to get something like this (started from Drupal root):
    sites/your_site_name/modules/dtc/plugins
  
  You can turn on the module at the administration pages:
     Administration / Site configuration / Dtc settings
     Direct path: admin/settings/dtc 
  Don't forget to set the correct permissions to be able to use the editor.   
  
  
===============================================================================
Hungarian section
===============================================================================
Telepítési útmutató magyarul

A DTC modul egy felületet biztosít a Drupal felhasználóknak, mely segítségével 
módosíthatják a Drupal oldalukra feltelepített sminkeket.

A modul megfelelő működéséhez szükséges kiegészítőket az első használat előtt 
telepíteni kell.
Szükséges Drupal modulok:
 * jQuery Update 6.x-2.x
 * jQuery UI, 1.7.x UI javascript library-val
A modulokon kívül szükség van még további fájlokra. Ezek a kiegészítők 
letölthetőek a http://drupalthemecreator.org/dtc-module oldalról. 
A Drupal 6-os verzióhoz a dtc-libs.zip fájlra lesz szükség. A zip fájl tartalmát
a dtc modul könyvtárába kell kitömöríteni.
Kitömörítés után a következő útvonalat kell kapnod: 
(a Drupal gyökeréből indulva)
  sites/oldal_neve/modules/dtc/plugins

A modul használatához először engedélyezni kell azt, majd az adminisztrációs
felületen található Dtc settings oldalon (admin/settings/dtc) lehet bekapcoslni.  
A szerkesztő csak a megfelelő jogosultságokkal használható. 

A smink szerkesztésének befejeztével a Generate gombra kattintva elmenthetőek az eddigi 
változtatások. Az elmentett fájl ki/bekapcsolható az adott smink beállításai között.
