<?php
// $Id$

/**
 * @file
 * The help strings, returns an information for the CSS properties.
 * Used as an AJAX callback.
 *
 * Copyright 2010 by Department of Software Engineering, University of Szeged, Hungary
 */

/**
 * Ajax callback. Used by the popup help system.
 * @param $param
 *  Name of the help text.
 */
function dtc_get_help($param) {

  $help = array();

  $help['border-radius'] = t("You can create rounded corners with this property. Warning! It's not yet supported by all browsers!");

  // Bacground settings help.
  $help['background-image'] = t('Sets the background image for an element.');
  $help['background-color'] = t('Sets the background color of an element.');
  $help['background-attachment'] = t('Sets whether a background image is fixed or scrolls with the rest of the page.');
  $help['background-repeat'] = t('Sets if/how a background image will be repeated.');
  $help['background-position-x'] = t('Sets the horizontal starting position of a background image.');
  $help['background-position-y'] = t('Sets the vertical starting position of a background image.');

  // Basic settings help.
  $help['text-decoration'] = t('Specifies the font style for a text and the decoration added to text.');
  $help['text-valign'] = t('Sets the vertical alignment of an element.');
  $help['text-align'] = t('Specifies the horizontal alignment of text in an element.');

  // Layout help.
  $help['layout-padding'] = t('The padding clears an area around the content (inside the border) of an element. The padding is affected by the background color of the element. The value is in pixel.');
  $help['layout-margin'] = t('The margin clears an area around an element (outside the border). The margin does not have a background color, and is completely transparent. The value is in pixel.');
  $help['layout-dimensions'] = t('Sets the width and height of an element.');
  $help['layout-float'] = t('Specifies whether or not a box (an element) should float.');
  $help['layout-clear'] = t('Specifies which sides of an element where other floating elements are not allowed.');
  $help['layout-display'] = t('Specifies the type of box an element should generate.');

  // Advanced help.
  $help['text-transform'] = t('Controls the capitalization of text.');
  $help['text-direction'] = t('Specifies the text direction/writing direction.');
  $help['advanced-indentation'] = t('Specifies the indentation of the first line in a text-block.');
  $help['advanced-word-spacing'] = t('Increases or decreases the white space between words.');
  $help['advanced-line-height'] = t('Specifies the line height.');
  $help['advanced-letter-spacing'] = t('Increases or decreases the space between characters in a text.');


  print $help[$param];
  exit(1);
}
